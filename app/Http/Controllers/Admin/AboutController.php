<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Admin;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class AboutController extends BasicController
{
    public function index()
    {
        $response = $this->AdminVariable();
        $response['aboutUs']=AboutUs::orderBy('id','desc')->get();
        return view('admin.page.about.list',compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'body'=>'required',
            'image'=>'required|image|max:2024',
        ]);

        $imageUrl = $this->uploadImage($request->file('image'),'about',[400]);

        $aboutUs = new AboutUs();


        $aboutUs->title   =   $request->title;
        $aboutUs->body    =   $request->body;
        $aboutUs->image   =   $imageUrl;

        try
        {
            $aboutUs->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }
        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function destroy(AboutUs $aboutUs)
    {
        $aboutUs->delete();
        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');
    }


}
