<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    // EDIT NAME AND MOBILE AND AVATAR
    public function edit()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['direction'] = 'direction-rtl';
        $response['inverse-direction'] = 'direction-ltr';
        $response['text-direction'] = 'text-right';
        $response['inverse-text'] = 'text-left';

        $response['font-size-0']= 'font-size-17';
        $response['font-size-1']= 'font-size-19';
        $response['font-size-2']= 'font-size-22';
        $response['font-size-3']= 'font-size-27';

        $response['admin']=Admin::firstWhere('id',Auth::user()->id);

        $response['page_title']='ویرایش مشخصات';
        return view('admin.page.admin.edit',compact('response'));
    }

    public function update(Request $request)
    {

        $admin=Admin::firstWhere('id',Auth::user()->id);

        $admin->name    =   $request->name;
        $admin->email   =   $request->email;

        try
        {
            $admin->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->back()->with('success','ویرایش با موفقیت انجام شد.');
    }


    // EDIT PASSWORD
    public function edit_passwrod()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['direction'] = 'direction-rtl';
        $response['inverse-direction'] = 'direction-ltr';
        $response['text-direction'] = 'text-right';
        $response['inverse-text'] = 'text-left';

        $response['font-size-0']= 'font-size-17';
        $response['font-size-1']= 'font-size-19';
        $response['font-size-2']= 'font-size-22';
        $response['font-size-3']= 'font-size-27';

        $response['page_title']='ویرایش کلمه عبور';
        return view('admin.page.admin.change-password',compact('response'));
    }

    public function update_passwrod(Request $request)
    {
        $request->validate([
            'old_password'=>'required',
            'password'=>'required|confirmed',
        ]);

        if (!Hash::check($request->old_password,Auth::user()->password))
        {
            return redirect()->back()->with('warning','کلمه عبور قبلی اشتباه می باشد.لطفا دوباره تلاش کنید');
        }


        $admin=Admin::firstWhere('id',Auth::user()->id);

        $admin->password=Hash::make($request->password);
        $admin->save();

        return redirect()->route('admin.logout')->with('کلمه عبور با موفقیت تغییر یافت');
    }

}
