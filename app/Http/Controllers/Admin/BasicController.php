<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class BasicController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function uploadImage($image,$direction,$sizes)
    {
        $imageUrl = [];
        foreach ($sizes as $size)
        {
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = 'storage/'.$direction.'/'.$size.'/'.$imageName;
            $img = Image::make($image->getRealPath());
            $img->resize($size)->save($destinationPath);
            $imageUrl[$size] = $destinationPath;
        }

        return $imageUrl;

    }
}
