<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Meta;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends BasicController
{

    public function index()
    {
        $response = $this->AdminVariable();

        $response['pages']=Page::where([['model','Category'],['type','blog']])->orderBy('id','DESC')->paginate(10);

        return view('admin.page.blog.list',compact('response'));
    }

    public function create()
    {
        $response = $this->AdminVariable();

        $response['category'] =Category::where('type','blog')->get();

        return view('admin.page.blog.add',compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title'       =>'required|unique:pages',
            'description' => 'required',
            'body'        => 'required',
            'image'       => 'required|image|max:2024',
            'category'    =>'required|exists:categories,id',
            'tags'        => 'required',
            'keyword'     => 'required',
        ]);

        $imageUrl = $this->uploadImage($request->file('image'),'page',[300,600,900]);

        $page=new Page();


        $page->title        =   $request->title;
        $page->model_id     =   $request->category;
        $page->model        =   'Category';
        $page->description  =   $request->description;
        $page->body         =   $request->body;
        $page->type         =   'blog';
        $page->writer       =   Auth::user()->id;
        $page->image        =   $imageUrl;

        if ($request->tags)
        {
            $page->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword)
        {
            $page->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }

        try
        {
            $page->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }



        return redirect()->route('admin.blog.list')->with('success','عملیات با موفقیت انجام شد.');

    }

    public function edit(Page $page)
    {
        $response = $this->AdminVariable();

        $response['page']    = $page;
        $response['category']= Category::where('type','blog')->get();

        return view('admin.page.blog.edit',compact('response'));
    }

    public function update(Request $request,Page $page)
    {

        $request->validate([
            'title'       =>'required|unique:pages,title,'.$page->id,
            'description' => 'required',
            'body'        => 'required',
            'category'    =>'required|exists:categories,id',
            'tags'        => 'required',
            'keyword'     => 'required',
        ]);

        if ($request->hasFile('image'))
        {
            $imageUrl = $this->uploadImage($request->file('image'),'page',[300,600,900]);
            $page->image        =   $imageUrl;
        }

        $page->title        =   $request->title;
        $page->model_id     =   $request->category;
        $page->model        =   'Category';
        $page->description  =   $request->description;
        $page->body         =   $request->body;
        $page->type         =   'blog';
        $page->writer       =   Auth::user()->id;

        if ($request->tags)
        {
            $page->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword)
        {
            $page->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }


        try
        {
            $page->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');
    }

    public function destroy(Page $page)
    {
        Meta::where([['model','Page'],['model_id',$page->id]])->delete();
        Visit::where([['model','Page'],['model_id',$page->id]])->delete();
        $page->delete();

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');

    }

}
