<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($type)
   {

       $response = $this->AdminVariable();

       $response['categories'] = Category::where('type',$type)->paginate(10);
       $response['type']       = $type;

       return view('admin.page.category.list',compact('response'));
   }

    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required|unique:categories',
            'icon' => 'required|image|max:2024',
        ]);

        $category = new Category();
        $category->title = $request->title;
        $category->type  = $request->type;
        $category->slug  = $request->slug;

        try
        {
            $category->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->back()->with('success','دسته با موفقیت اضافه گردید.');

    }

    public function edit($id)
    {
        $response = $this->AdminVariable();
        $response['category']   =   Category::firstWhere('id',$id);
        return view('admin.page.category.edit',compact('response'));
    }

    public function update(Request $request,$id)
    {
        $category=Category::firstWhere('id',$id);

        $request->validate([
            'title'=>'required|unique:categories,title,'.$category->id,
        ]);

        $category->title = $request->title;
        $category->type  = $request->type;

        try
        {
            $category->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','دسته با موفقیت ویرایش شد.');
    }

    public function destroy($id)
    {
        $category=Category::firstWhere('id',$id);

        //delete meta for this category
        $category->Meta()->delete();
        $category->pivotTruncate();

        try
        {
            $category->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->back()->with('success','دسته با موفقیت حذف شد.');
    }


}
