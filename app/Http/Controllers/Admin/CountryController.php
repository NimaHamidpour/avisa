<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CountryController extends BasicController
{
    public function index()
    {
        $response = $this->AdminVariable();

        $response['country'] = Country::orderBy('title', 'DESC')->paginate(15);

        return view('admin.page.country.list', compact('response'));
    }

    public function create()
    {
        $response = $this->AdminVariable();
        return view('admin.page.country.add', compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:countries',
            'description' => 'required',
            'body' => 'required',
            'image' => 'required|image|max:2024',
            'tags' => 'required',
            'keyword' => 'required',
        ]);

        $imageUrl = $this->uploadImage($request->file('image'), 'country', [100, 400, 800]);

        $country = new Country();
        $country->title = $request->title;
        $country->description = $request->description;
        $country->body = $request->body;
        $country->writer = Auth::user()->id;
        $country->image = $imageUrl;

        if ($request->tags) {
            $country->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword) {
            $country->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }

        try {
            $country->save();
        } catch (Exception $exception) {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->route('admin.country.list')->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function edit(Country $country)
    {
        $response = $this->AdminVariable();
        $response['country'] = $country;
        return view('admin.page.country.edit', compact('response'));
    }

    public function update(Request $request, Country $country)
    {
        $request->validate([
            'title' => 'required|unique:countries,title,' . $country->id,
            'description' => 'required',
            'body' => 'required',
            'tags' => 'required',
            'keyword' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $imageUrl = $this->uploadImage($request->file('image'), 'country', [100, 400, 800]);
            $country->image = $imageUrl;
        }

        $country->title = $request->title;
        $country->description = $request->description;
        $country->body = $request->body;
        $country->writer = Auth::user()->id;

        if ($request->tags) {
            $country->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword) {
            $country->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }

        try {
            $country->save();
        } catch (Exception $exception) {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->route('admin.country.list')->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function destroy(Country $country)
    {
        Visit::where([['model', 'Country'], ['model_id', $country->id]])->delete();
        $country->delete();
        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');

    }


}
