<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use App\Models\Setting;
use Illuminate\Http\Request;

class ImageController extends BasicController
{

    public function index()
    {
        $response = $this->AdminVariable();
        $response['image']  = Image::orderBy('key','ASC')->paginate(15);
        $response['parent'] = Setting::where('type','image')->get();
        return view('admin.page.image.list',compact('response'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required|image|max:2048',
            'title'=>'required',
        ]);


        $parent = Setting::firstWhere('key',$request->parent);

        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('image'))
        {
            $imageUrl = $this->uploadImage($request->file('image'),'image',[$parent->value]);
        }

        $image             = new Image();
        $image->src        = $imageUrl;
        $image->title      = $request->title;
        $image->url        = $request->url;
        $image->key        = $parent->key;

        $image->save();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function edit(Image $image)
    {
        $response = $this->AdminVariable();
        $response['image']  = $image;
        $response['parent'] = Setting::where('type','image')->get();

        return view('admin.page.image.edit',compact('response'));
    }

    public function update(Request $request,Image $image)
    {
        $request->validate(['title'=>'required']);

        $parent = Setting::firstWhere('key',$request->parent);

        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('image'))
        {
            $imageUrl = $this->uploadImage($request->file('image'),'image',[$parent->value]);
            $image->src   = $imageUrl;

        }

        $image->title = $request->title;
        $image->key   = $parent->key;
        $image->url   = $request->url;

        $image->save();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function destroy(Image $image)
    {
        try
        {
            $image->delete();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning', $exception->getCode());
        }
        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');

    }
}
