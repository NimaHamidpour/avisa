<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Page;
use App\Models\Service;
use App\Models\Setting;
use App\Models\User;
use App\Models\Visit;
use Carbon\Carbon;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $response = $this->AdminVariable();

        $response['user-count']     = User::all()->count();
        $response['blog-count']     = Page::where('type','blog')->get()->count();
        $response['country-count']  = Country::all()->count();
        $response['service-count']  = Service::all()->count();
        $response['webpage-count']  = Page::where('type','page')->get()->count();


        //  VIEWS
        for ($i=0;$i < 11 ;$i++)
        {
            $response['view_count'][$i]=Visit::whereDate('created_at',Carbon::now()->subDays($i)
                ->toDateString())->get()->count();
            $response['view_date'][$i]=jdate(Carbon::now()->subDays($i)->toDateString())->format('Y/m/d');
        }
        $response['view_all']=Visit::all()->count();
        $response['max_view_count']=max($response['view_count']);



        return view('admin.page.dashboard.dashboard',compact('response'));
    }

}
