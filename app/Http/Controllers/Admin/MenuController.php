<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Setting;
use Illuminate\Http\Request;

class MenuController extends BasicController
{



    public function index()
   {
       $response = $this->AdminVariable();

       $response['menu'] = Menu::orderBy('id','ASC')->paginate(15);
       $response['parent'] = Menu::orderBy('title','ASC')->get();

       return view('admin.page.menu.list',compact('response'));
   }

    public function store(Request $request)
    {
        $request->validate(['title'=>'required']);

        $menu = new Menu();
        $menu->title     = $request->title;
        $menu->url       = $request->url;
        $menu->position  = $request->position;
        $menu->parent    = $request->parent;

        try{
            $menu->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');

    }

    public function edit(Menu $menu)
    {
        $response = $this->AdminVariable();

        $response['menu']=$menu;
        $response['parent'] = Menu::where('id','<>',$menu->id)->orderBy('title','ASC')->get();

        return view('admin.page.menu.edit',compact('response'));
    }

    public function update(Request $request,Menu $menu)
    {
        $request->validate(['title'=>'required']);

        $menu->title     = $request->title;
        $menu->url       = $request->url;
        $menu->position  = $request->position;
        $menu->parent    = $request->parent;

        try{
            $menu->save();
        }
        catch (Exception $exception)
        {
            return redirect()->back()->with('warning',$exception->getCode());
        }

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');
    }

    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect()->back()->with('success','عملیات با موفقیت انجام شد.');
    }


}
