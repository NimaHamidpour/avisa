<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Country;
use App\Models\Service;
use App\Models\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ServiceController extends BasicController
{
    public function index()
    {
        $response = $this->AdminVariable();

        $response['service'] = Service::orderBy('title', 'DESC')->paginate(15);

        return view('admin.page.service.list', compact('response'));
    }

    public function create()
    {
        $response = $this->AdminVariable();
        $response['country']   = Country::orderBy('title','asc')->get();
        $response['category']  = Category::where('type','service')->orderBy('title','asc')->get();
        return view('admin.page.service.add', compact('response'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:countries',
            'description' => 'required',
            'body' => 'required',
            'image' => 'required|image|max:2024',
            'tags' => 'required',
            'keyword' => 'required',
        ]);

        $imageUrl = $this->uploadImage($request->file('image'), 'service', [300, 600, 900]);

        $service = new Service();
        $service->title       = $request->title;
        $service->description = $request->description;
        $service->body        = $request->body;
        $service->country     = $request->country;
        $service->category    = $request->category;
        $service->writer      = Auth::user()->id;
        $service->image       = $imageUrl;

        if ($request->tags) {
            $service->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword) {
            $service->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }

        try {
            $service->save();
        } catch (Exception $exception) {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->route('admin.service.list')->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function edit(Service $service)
    {
        $response = $this->AdminVariable();
        $response['service']   = $service;
        $response['country']   = Country::orderBy('title','asc')->get();
        $response['category']  = Category::where('type','service')->orderBy('title','asc')->get();
        return view('admin.page.service.edit', compact('response'));
    }

    public function update(Request $request, Service $service)
    {
        $request->validate([
            'title' => 'required|unique:services,title,' . $service->id,
            'description' => 'required',
            'body' => 'required',
            'tags' => 'required',
            'keyword' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $imageUrl = $this->uploadImage($request->file('image'), 'service', [300, 600, 900]);
            $service->image = $imageUrl;
        }

        $service->title       = $request->title;
        $service->description = $request->description;
        $service->body        = $request->body;
        $service->country     = $request->country;
        $service->category    = $request->category;
        $service->writer      = Auth::user()->id;

        if ($request->tags) {
            $service->tags = implode(', ', array_column(json_decode($request->tags), 'value'));
        }

        if ($request->keyword) {
            $service->keyword = implode(', ', array_column(json_decode($request->keyword), 'value'));
        }

        try
        {
            $service->save();
        } catch (Exception $exception) {
            return redirect()->back()->with('warning', $exception->getCode());
        }

        return redirect()->route('admin.service.list')->with('success', 'عملیات با موفقیت انجام شد.');
    }

    public function destroy(Service $service)
    {
        Visit::where([['model', 'Service'], ['model_id', $service->id]])->delete();
        $service->delete();
        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد.');

    }


}
