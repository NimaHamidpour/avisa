<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function edit()
    {

        $response = [];

        $response = $this->AdminVariable();

        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_mobile']=optional(Setting::firstWhere('key','app_mobile'))->value;

        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_slogan']=optional(Setting::firstWhere('key','app_slogan'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;



        return view('admin.page.setting.setting',compact('response'));
    }

    public function update(Request $request)
    {

        // UPDATE LOGO TO STORAGE/SETTING
        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('storage\setting');
//            $destinationPath = 'storage/setting';
            $img =\Intervention\Image\Facades\Image::make($image->getRealPath());
            $img->fit(200)->save($destinationPath. $input['imagename']);

            /* APP LOGO */
            $app_logo=Setting::firstWhere('key','app_logo');
            if ($app_logo!='')
            {
                $app_logo->value='storage/setting'.$input['imagename'];
            }
            else
            {
                $app_logo=new Setting([
                    'key'=>'app_logo',
                    'value'=>'storage/setting/'.$input['imagename'],
                    'title'=>'لوگو سایت',
                ]);
            }
            $app_logo->save();

            // UPDATE LOGO TO STORAGE/SETTING
        }


        /* Name */
        $this->updateSettingInfo('app_name',$request->title,'عنوان سایت');

        /* Issue */
        $this->updateSettingInfo('app_slogan',$request->slogan,'موضوع سایت');

        /* Address */
        $this->updateSettingInfo('app_address',$request->address,'آدرس سایت');

        /*  Tells and Mobiles */
        $this->updateSettingInfo('app_tell',$request->tell,'تلفن سایت');


        /*  Description */
        $this->updateSettingInfo('app_description',$request->description,'درباره ما');





        return redirect()->back()->with('ویرایش با موفقیت انجام شد');
    }

    public function updateSettingInfo($key,$title,$help)
    {
        $info   =   Setting::firstWhere( 'key' , $key );
        if ($info !== null)
        {
            $info->value  =  $title;
        }
        else
        {
            $info = new Setting();
            $info->key   = $key;
            $info->value = $title;
            $info->value = $help;
        }
        $info->save();

        return true;
    }
}
