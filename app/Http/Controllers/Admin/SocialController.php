<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use App\Models\Setting;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $response=[];

        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['direction'] = 'direction-rtl';
        $response['inverse-direction'] = 'direction-ltr';
        $response['text-direction'] = 'text-right';
        $response['inverse-text'] = 'text-left';

        $response['font-size-0']= 'font-size-17';
        $response['font-size-1']= 'font-size-19';
        $response['font-size-2']= 'font-size-22';
        $response['font-size-3']= 'font-size-27';

        $social = Setting::firstWhere('key','social');
        $response['socials']=Meta::where([['model','Setting'],['model_id',$social->id]])->paginate(10);

        $response['page_title']='شبکه های اجتماعی';
        return view('admin.page.setting.social',compact('response'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'link'=>'required',
        ]);

        $social = Setting::firstWhere('key','social');

        $social->setMeta([$request->type=>$request->link],true,$request->title);

        return redirect()->back()->with('success', 'شبکه اجتماعی جدید با موفقیت اضافه گردید ');
    }


    public function destroy(Meta $meta)
    {
        try {
            $meta->delete();
        } catch (Exception $exception) {
            return redirect()->back()->with('warning', $exception->getCode());
        }
        return redirect()->back()->with('success', 'شبکه اجتماعی مورد نظر با موفقیت حذف گردید ');

    }
}
