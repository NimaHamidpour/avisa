<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['users']=User::orderBy('id','DESC')->paginate(15);

        $response['direction'] = 'direction-rtl';
        $response['inverse-direction'] = 'direction-ltr';
        $response['text-direction'] = 'text-right';
        $response['inverse-text'] = 'text-left';

        $response['font-size-0']= 'font-size-17';
        $response['font-size-1']= 'font-size-19';
        $response['font-size-2']= 'font-size-22';
        $response['font-size-3']= 'font-size-27';

        $response['page_title']='لیست کاربران';
        return view('admin.page.user.list',compact('response'));
    }

    public function show(User $user)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $response['user']=$user;

        $response['direction'] = 'direction-rtl';
        $response['inverse-direction'] = 'direction-ltr';
        $response['text-direction'] = 'text-right';
        $response['inverse-text'] = 'text-left';

        $response['font-size-0']= 'font-size-17';
        $response['font-size-1']= 'font-size-19';
        $response['font-size-2']= 'font-size-22';
        $response['font-size-3']= 'font-size-27';

        $response['page_title']='مشخصات  : '.$user->name;

        return view('admin.page.user.detail',compact('response'));
    }



    public function search(Request $request)
    {
        $response=[];
        $response['app_name']=optional(Setting::firstWhere('key','app_name_fa'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        $user=User::orderBy('id','DESC');

        if (isset($request->name) && $request->name !== null)
        {
            $user=$user->where('name','LIKE','%'.$request->name.'%');
            $response['user_name']=$request->name;

        }

        if (isset($request->mobile) && $request->mobile !== null)
        {
            $user=$user->where('mobile','LIKE','%'.$request->mobile.'%');
            $response['user_mobile']=$request->mobile;

        }

        if (isset($request->sex) && $request->sex !== null && $request->sex !== 'all')
        {
            $user=$user->where('sex',$request->sex);
            $response['user_sex']=$request->sex;
        }


        if (isset($request->degree) && $request->degree !== null && $request->degree !== 'all')
        {
            $user=$user->where('degree',$request->degree);
            $response['user_degree']=$request->degree;
        }

        $response['users']=$user->paginate(10);

        $response['page_title']='جستجوی دانش پذیران';
        return view('admin.page.user.list',compact('response'));
    }


}
