<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function blogGroups(Request $request)
    {
        $blogs=Page::where([['type','blog'],['model_id',$request->id]])->get();

        $response='<div class="carousel owl-carousel" id="blog-carousel">';
        foreach ($blogs as $blog)
        {
            $response.='<div class="item">
                                <a class="text-decoration-none"
                                    href="'.route('app.blog.show',$blog->slug).'">
                                    <figure>
                                        <img src="'.$blog->image['300'].'" alt="'.$blog->title.'">
                                    </figure>
                                    <h2 class="title text-center" style="min-height: 45px">'.$blog->title.'</h2>
                                    <p class="desc text-justify">
                                     '.mb_substr(strip_tags($blog->description),0,200,'UTF8').'...
                                    </p>
                                    <div class="date">'.jdate($blog->created_at)->format('Y %B').'</div>
                                </a>
                            </div>';
        }
        $response.='</div>';
        return $response;

    }

    public function search(Request $request)
    {
        $blogs=Page::where('title','LIKE','%'.$request->search.'%')
            ->orderBy('id','desc')->limit(5)->get();

        $output='';

        foreach ($blogs as $blog)
        {
            $output.='<div class="card border-bottom mt-2">
                                <a class="w-100 d-content text-decoration-none cursor-pointer blog-hover"
                                 href="'.route('app.blog.show',$blog->slug).'">
                                    <div class="row no-gutters">
                                        <!-- IMG -->
                                        <div class="col-4">
                                            <img src="'.url($blog->image['300']).'" style="height: 70px!important;margin-top:10px" class="card-img height-100" alt="'.$blog->title.'">
                                        </div>

                                        <!-- TEXT -->
                                        <div class="col-8">
                                            <div class="card-body" style="padding: 0.75rem!important;">
                                                <h2 class="card-title text-right font-size-14 font-family-yekan text-black">'.$blog->title.'</h2>
                                                <div class="col-12 text-right my-2 mx-0 px-0">
                                                    <a class="blog-category-link font-family-yekan" href="'.route('app.blog.show',$blog->Category->slug).'">
                                                       '.$blog->Category->title.'
                                                    </a>
                                                    <span class="card-title text-right font-size-11 text-secondary font-family-iransans mr-1">
                                                            '.jdate($blog->created_at)->format('Y.m.d').'
                                                     </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>';
        }

        echo $output;
    }

}
