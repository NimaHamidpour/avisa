<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


class CaptchaController extends Controller
{
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('math')]);
    }
}
