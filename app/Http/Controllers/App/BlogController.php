<?php

namespace App\Http\Controllers\App;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Meta;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Visit;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index($slug=null)
    {
        $response = $this->baseVariable();


        // GET CATEGORY EXIST
        $response['categories']=Category::where('type','blog')->get();


        // GET MOST VIEW BLOG
        $response['most_visited']=Page::where('type','blog')->orderBy('view_count','desc')->limit(3)->get();


        // GET BLOG --> IF SEARCH CATEGORY GET BLOG WITH SELECTED CATEGORY IF NOT GET ALL
        if ($slug !== null)
        {
            $category=Category::firstWhere('slug',$slug);
            $response['blogItem']=Page::where('type','blog')
                                ->whereNotIN('id',$response['most_visited']->pluck('id'))
                                ->where('model_id',$category->id)
                                ->orderBy('id','desc')->get();
        }
        else
        {
            $response['blogItem']=Page::where('type','blog')
                                ->whereNotIN('id',$response['most_visited']->pluck('id'))
                                ->orderBy('id','desc')->get();
        }



        return view('blog.page.main',compact('response'));
    }

    public function show(Page $page)
    {

        $response = $this->baseVariable();

        // GET CATEGORY EXIST
        $response['categories']=Category::where('type','blog')->get();


        // GET BLOG
        $response['blog']=$page;
        $response['blog']->view_count++;
        $response['blog']->save();

        $response['related_blog']  =Page::where('type','blog')->where('model_id',$page->model_id)->orderBy('id','desc')->limit(3)->get();

        // GET MOST VIEW BLOG
        $response['most_visited']=Page::where('type','blog')->orderBy('view_count','desc')->limit(3)->get();


        return view('blog.page.article',compact('response'));
    }

    public function search(Request $request)
    {
        $response = $this->baseVariable();

        // GET CATEGORY EXIST
        $response['categories']=Category::where('type','blog')->get();


        // GET SEARCH TEXT AND BLOG
        $response['search']=$request->search_text_name;

        $response['blogs']=Page::where([['type','blog'],['title','LIKE',$request->search_text_name.'%']])
            ->orderBy('id','desc')->get();



        // GET MOST VIEW BLOG
        $response['most_visited']=Page::where('type','blog')->orderBy('view_count','desc')->limit(3)->get();


        return view('blog.page.search',compact('response'));
    }


}
