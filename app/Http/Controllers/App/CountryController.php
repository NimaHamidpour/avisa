<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Country;



class CountryController extends Controller
{
    public function show(Country $country)
    {

        $response = $this->baseVariable();
        $response['country'] = $country;
        return view('app.page.country.show',compact('response'));
    }
}
