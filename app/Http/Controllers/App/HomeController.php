<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use App\Models\Category;
use App\Models\Image;
use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;


class HomeController extends Controller
{
    public function index()
    {
        $response = $this->baseVariable();

        /*
         * Main Slider
         */
        if (Cache::has('mainSlider'))
        {
            $response['slider'] = Cache::get('mainSlider');
        } else {
            $response['slider'] = Image::where('key', 'MainSlider')->orderBy('id', 'desc')->limit(4)->get();
            Cache::put('mainSlider', $response['slider'], Carbon::now()->addDays(10));
        }


        /*
         * Blog
         */
        if (Cache::has('blogGroup'))
        {
            $response['blogGroup'] = Cache::get('blogGroup');
        }
        else
        {
            // BLOG
            $response['blogGroup'] = Category::where('type', 'blog')->orderBy('id', 'asc')->get();
            Cache::put('blogGroup', $response['blogGroup'], Carbon::now()->addDays(7));
        }
        if (count($response['blogGroup']) > 0)
        {
            $response['blogItem']=Page::where([['type','blog'],['model_id',$response['blogGroup']->first()->id]])
                ->orderBy('id','desc')->limit(6)->get();
        }

        /*
         * AboutUs
        */
        if (Cache::has('aboutUs'))
        {
            $response['aboutUs'] = Cache::get('aboutUs');
        }
        else
        {
            $response['aboutUs'] = AboutUs::orderBy('id','desc')->limit(4)->get();
            Cache::put('aboutUs', $response['aboutUs'], Carbon::now()->addDays(7));
        }


        return view('app.page.home', compact('response'));
    }


}
