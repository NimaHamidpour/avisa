<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Filesystem\Cache;

class PageController extends Controller
{
    public function show(Page $page)
    {

        $response = $this->baseVariable();
        $response['page'] = $page;
        return view('app.page.page',compact('response'));
    }
}
