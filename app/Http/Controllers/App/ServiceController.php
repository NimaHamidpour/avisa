<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Filesystem\Cache;


class ServiceController extends Controller
{
    public function show(Service $service)
    {

        $response = $this->baseVariable();
        $response['service'] = $service;
        return view('app.page.service.show',compact('response'));
    }
}
