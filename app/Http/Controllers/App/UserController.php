<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Meta;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;


class UserController extends Controller
{

    public function create()
    {
        $response=[];

        $response['lang']   =   App::getLocale();

        // **** SETTINGS OF SITE ****
        $response['app_name']           =   optional(Setting::firstWhere('key','app_name_'.$response['lang']))->value;
        $response['app_slogan']         =   optional(Setting::firstWhere('key','app_slogan_'.$response['lang']))->value;
        $response['app_short_text']     =   optional(Setting::firstWhere('key','app_short_text_'.$response['lang']))->value;
        $response['app_description']    =   optional(Setting::firstWhere('key','app_description_'.$response['lang']))->value;
        $response['app_issue']          =   optional(Setting::firstWhere('key','app_issue_'.$response['lang']))->value;
        $response['app_address']        =   optional(Setting::firstWhere('key','app_address_'.$response['lang']))->value;

        $response['app_tell']           =   optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_mobile']         =   optional(Setting::firstWhere('key','app_mobile'))->value;
        $response['app_logo']           =   optional(Setting::firstWhere('key','app_logo'))->value;

        $social                         =   Setting::firstWhere('key','social');
        $response['socials']            =   Meta::where([['model','Setting'],['model_id',$social->id]])->orderBy('key','ASC')->get();


        if ($response['lang'] === 'en')
        {
            $response['direction'] = 'direction-ltr';
            $response['inverse-direction'] = 'direction-rtl';
            $response['text-direction'] = 'text-left';
            $response['inverse-text'] = 'text-right';

            $response['font-size-0']= 'font-size-15';
            $response['font-size-1']= 'font-size-17';
            $response['font-size-2']= 'font-size-19';
            $response['font-size-3']= 'font-size-21';
            $response['font-size-4']= 'font-size-27';

            $response['font-family-main']     = 'font-family-cambria';
            $response['font-family-second']   = 'font-family-montserrat';
            $response['font-family-number']   = 'font-family-aviny';
        }
        else
        {
            $response['direction'] = 'direction-rtl';
            $response['inverse-direction'] = 'direction-ltr';
            $response['text-direction'] = 'text-right';
            $response['inverse-text'] = 'text-left';

            $response['font-size-0']= 'font-size-14';
            $response['font-size-1']= 'font-size-17';
            $response['font-size-2']= 'font-size-19';
            $response['font-size-3']= 'font-size-21';
            $response['font-size-4']= 'font-size-25';

            $response['font-family-main']   = 'font-family-yekan';
            $response['font-family-second']   = 'font-family-iransanse';
            $response['font-family-number'] = 'font-family-aviny';
        }


        return view('app.page.register',compact('response'));
    }



    public function store(Request $request)
    {

        $request->validate(
            [
                'name' =>['required', 'string', 'max:255'],
                'mobile' => ['required', 'string', 'digits:11', 'unique:users'],
                'child' => ['required','numeric'],
                'experience' => ['required','numeric'],
                'field' => ['required', 'string', 'max:255'],
                'year' => ['required','numeric','digits:4'],
                'month' => ['required','numeric'],
                'day' => ['required','numeric'],
            ]
        );

        $user = new User();

        $user->name        = $request->name;
        $user->mobile      = $request->mobile;
        $user->email       = $request->email;
        $user->city        = $request->city;
        $user->sex         = $request->sex;
        $user->degree      = $request->degree;
        $user->field       = $request->field;
        $user->married     = $request->married;
        $user->child       = $request->child;
        $user->birthday    = Jalalian::fromFormat('Y-m-d',$request->year.'-'.$request->month.'-'.$request->day)->toCarbon()->toDateString();
        $user->job         = $request->job;
        $user->experience  = $request->experience;


        if ($request->hasFile('resume'))
        {
            $resume = $request->file('resume');
            $input['resumeName'] = time().'.'.$resume->getClientOriginalExtension();

            $destinationPath = 'storage/resume/';
            move_uploaded_file($resume,$destinationPath . $input['resumeName']);

            $user->resume  = 'storage/resume/'.$input['resumeName'];
        }


        $user->save();

        if ( App::getLocale() === 'fa')
             return redirect()->back()->with('success','فرم ارزیابی شما با موفقیت ثبت شد.در اسرع وقت با شما تماس گرفته میشود.');
        else
            return redirect()->back()->with('success','Your evaluation form has been successfully registered. You will be contacted as soon as possible.');

    }
}
