<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AdminController extends Controller
{
    public function show()
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        return view('auth.admin.login',compact('response'));
    }

    public function submit(Request $request){

        //validate login form
        $request->validate([
            'email'   => 'required|email',
            'password' => 'required|min:4',
            'Capcha'=>'required|captcha'

        ]);

        // CHECK LOGIN
        if (Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remmember))
        {
            // REDIRECT IF SUCCESS
            return redirect()->intended(route('admin.dashboard'));
        }

        // REDIRECT IF FAILD
        return redirect()->back()->withInput($request->only('mobile','remmember'));
    }


    public function forgetform()
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        return view('auth.admin.forget',compact('response'));
    }

    public function forget(Request $request)
    {
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;

        $request->validate([
            'email'=>'required|email',
        ]);

        $admin=Admin::firstWhere('email',$request->email);

        if ($admin !== null)
        {
            $code = rand(10000, 99999);

            $admin->deleteMeta('password_reset_code');

            $admin->setMeta([
                'password_reset_code' => $code
            ]);

            $response=[
                'name'=>$admin->name,
                'email'=>$admin->email,
                'code'=>$code,
                'reset_url'=>'https://mohajer.ir/admin/reset/'.$admin->email,
                'subject'=>'فرامــوشی رمز عبور',
                'view'=>'mail.ForgetPassword'
            ];

            SendEmailJob::dispatch($response);
//            Smsirlaravel::ultraFastSend(['name'=>$admin->name,'code'=>$code,'site'=>$response['app_name']],31512,$admin->mobile);
//

            return redirect()->route('admin.reset.form',$admin->email)->with('success','کلمه عبور موقت به ایمیل شما ارسال گردید');
        }
        else
        {
            return redirect()->back()->with('warning','ایمیل وارد شده معتبر نمی باشد . لطفا دوباره امتحان کنید');
        }

    }


    //***   reset  ***/
    public function resetform($email)
    {
        // **** SETTINGS OF SITE ****
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['email']=$email;
        return view('auth.admin.reset',compact('response'));
    }

    public function reset(Request $request)
    {
        $request->validate([
            'token_code' => 'required|digits:5',
            'password' => 'required|string|confirmed|min:5|max:191'
        ]);

        if (isset($request->email) && $request->email !== null)
        {
            $admin = Admin::firstWhere('email',$request->email);
        }

        abort_if((int) $admin->getMeta('password_reset_code') !== (int) $request->token_code, 400, trans('passwords.token'));

        $admin->deleteMeta('password_reset_code');


        $admin->password=bcrypt($request->password);

        $admin->save();

        return redirect()->route('admin.login.show')->with('success','رمز عبور با موفقیت تغییر یافت');
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        Session::flush();
        return redirect()->route('app.home');
    }

}
