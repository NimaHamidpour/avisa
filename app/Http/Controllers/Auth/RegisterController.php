<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    use RegistersUsers;


    protected $redirectTo = RouteServiceProvider::HOME;


    public function __construct()
    {
        $this->middleware('guest');
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'codemeli' => ['required', 'numeric', 'digits:10', 'unique:users'],
            'mobile' => ['required', 'string', 'digits:11', 'unique:users'],
            'address' => ['required', 'string', 'max:255'],
            'birthday' => ['required','numeric'],
            'tell' => ['required', 'string', 'digits:11'],
            'field' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }


    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'codemeli' => $data['codemeli'],
            'mobile' => $data['mobile'],
            'address' => $data['address'],
            'sex' => $data['sex'],
            'degree' => $data['degree'],
            'birthday' => $data['birthday'],
            'tell' => $data['tell'],
            'field' => $data['field'],
            'password' => Hash::make($data['password']),
            'writer' => 'User',

        ]);
    }
}
