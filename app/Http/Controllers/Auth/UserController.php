<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;
use App\Jobs\SendVerificationEmailJob;
use App\Mail\EmailForQueuing;
use App\Models\Admin;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Ipecompany\Smsirlaravel\Smsirlaravel;

class UserController extends Controller
{

    public function forgetform()
    {
        $response=[];
        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_main_tell']=optional(Setting::firstWhere('key','app_main_tell'))->value;
        $response['app_main_email']=optional(Setting::firstWhere('key','app_main_email'))->value;
        $response['socials']=Setting::where('type','social')->get();
        $response['background']='0D0A29';
        return view('auth.user.forget',compact('response'));
    }

    public function forget(Request $request)
    {
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;

        $request->validate([
            'mobile'=>'required|digits:11',
        ]);

        $user=User::firstWhere('mobile',$request->mobile);

        if ($user !== null)
        {
            $code = rand(10000, 99999);

            $user->deleteMeta('password_reset_code');

            $user->setMeta([
                'password_reset_code' => $code
            ]);

//            $response=[
//                'name'=>$user->name,
//                'email'=>$user->email,
//                'code'=>$code,
//                'reset_url'=>'https://fxclimax.ir/user/reset/'.$user->email,
//                'subject'=>'فرامــوشی رمز عبور',
//                'view'=>'mail.ForgetPassword'
//            ];
//            SendEmailJob::dispatch($response);

            Smsirlaravel::ultraFastSend(['name'=>$user->name,'code'=>$code,'site'=>$response['app_name']],31512,$user->mobile);
            return redirect()->route('user.reset.form',$user->mobile)->with('success','کد بازیابی کلمه عبور  به شماره تماس شما ارسال گردید');
        }
        else
        {
            return redirect()->back()->with('warning','ایمیل وارد شده معتبر نمی باشد . لطفا دوباره امتحان کنید');
        }

    }


    //***   reset  ***/
    public function resetform($mobile)
    {
        $response=[];
        // SETTINGS OF SITE
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_short_text']=optional(Setting::firstWhere('key','app_short_text'))->value;
        $response['app_description']=optional(Setting::firstWhere('key','app_description'))->value;
        $response['app_issue']=optional(Setting::firstWhere('key','app_issue'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;
        $response['app_address']=optional(Setting::firstWhere('key','app_address'))->value;
        $response['app_tell']=optional(Setting::firstWhere('key','app_tell'))->value;
        $response['app_main_tell']=optional(Setting::firstWhere('key','app_main_tell'))->value;
        $response['app_main_email']=optional(Setting::firstWhere('key','app_main_email'))->value;
        $response['socials']=Setting::where('type','social')->get();

        $response['mobile']=$mobile;
        $response['background']='0D0A29';
        return view('auth.user.reset',compact('response'));
    }

    public function reset(Request $request)
    {
        $request->validate([
            'token_code' => 'required|digits:5',
            'password' => 'required|string|confirmed|min:5|max:191'
        ]);
        if (isset($request->mobile) && $request->mobile !== null)
        {
            $user = User::firstWhere('mobile',$request->mobile);
        }

        abort_if((int) $user->getMeta('password_reset_code') !== (int) $request->token_code, 400, trans('passwords.token'));

        $user->deleteMeta('password_reset_code');


        $user->password=bcrypt($request->password);

        $user->save();

        return redirect()->route('login')->with('success','رمز عبور با موفقیت تغییر یافت');
    }

}
