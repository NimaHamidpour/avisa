<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\Menu;
use App\Models\Meta;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function AdminVariable()
    {
        $response = [];
        $response['app_name']=optional(Setting::firstWhere('key','app_name'))->value;
        $response['app_logo']=optional(Setting::firstWhere('key','app_logo'))->value;

        return $response;
    }

    public function baseVariable()
    {

        $response = [];

        if (Cache::has('app_name'))
        {
            $response['app_name']        = Cache::get('app_name');
            $response['app_slogan']       = Cache::get('app_slogan');
            $response['app_logo']        = Cache::get('app_logo');
            $response['app_address']     = Cache::get('app_address');
            $response['app_tell']        = Cache::get('app_tell');
            $response['app_description'] = Cache::get('app_description');
            $response['socials']         = Cache::get('socials');
            $response['menu']            = Cache::get('menu');
            $response['serviceGroup']    = Cache::get('serviceGroup');
            $response['countries']       = Cache::get('countries');
        }
        else
        {
            $social = Setting::firstWhere('key', 'social');
            $response['socials'] = Meta::where([['model', 'Setting'], ['model_id', $social->id]])->orderBy('key', 'ASC')->get();

            $response['app_name']        = optional(Setting::firstWhere('key', 'app_name'))->value;
            $response['app_slogan']       = optional(Setting::firstWhere('key', 'app_slogan'))->value;
            $response['app_logo']        = optional(Setting::firstWhere('key', 'app_logo'))->value;
            $response['app_address']     = optional(Setting::firstWhere('key', 'app_address'))->value;
            $response['app_tell']        = optional(Setting::firstWhere('key', 'app_tell'))->value;
            $response['app_description'] = optional(Setting::firstWhere('key', 'app_description'))->value;

            $response['menu']            = Menu::where('parent',null)->orderBy('id','asc')->get();

            $response['serviceGroup'] = Category::where('type','service')->orderBy('title','asc')->with('Service')->get();

            $response['countries'] = Country::orderBy('title','asc')->get();

            // Cache Information
            Cache::put('app_name',$response['app_name'],Carbon::now()->addDays(100));
            Cache::put('app_slogan',$response['app_slogan'],Carbon::now()->addDays(100));
            Cache::put('app_logo',$response['app_logo'],Carbon::now()->addDays(100));
            Cache::put('app_address',$response['app_address'],Carbon::now()->addDays(100));
            Cache::put('app_tell',$response['app_tell'],Carbon::now()->addDays(100));
            Cache::put('app_description',$response['app_description'],Carbon::now()->addDays(100));
            Cache::put('socials',$response['socials'],Carbon::now()->addDays(100));
            Cache::put('menu',$response['menu'],Carbon::now()->addDays(7));

            Cache::put('serviceGroup',$response['serviceGroup'],Carbon::now()->addDays(3));
            Cache::put('countries',$response['countries'],Carbon::now()->addDays(3));

        }


        return $response;

    }


}
