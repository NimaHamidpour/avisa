<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return  mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        abort_if(!optional(Auth::user())->hasPermission($permission), 403, trans('شما دسترسی به این بخش ندارید'));

        return $next($request);
    }
}
