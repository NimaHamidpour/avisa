<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CacheJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * @var int
     */
    public $timeout = 120;

    /**
     * @var string $key
     */
    protected $key;

    /**
     * @param  int|\Carbon\Carbon $expire
     */
    protected $expire;

    /**
     * @param  string $method
     */
    protected $method;

    /**
     * @param  array $params
     */
    protected $params;

    /**
     * @param  array $tags
     */
    protected $tags;

    /**
     * Create a new job instance.
     *
     * @param  string $key
     * @param  int|\Carbon\Carbon $expire
     * @param  string|null $method
     * @param  array $params
     * @param  array $tags
     *
     * @return  void
     */
    public function __construct(string $key, $expire = 3600, $method = null, array $params = [], array $tags = [])
    {
        $this->key = $key;
        $this->expire = $expire;
        $this->method = $method;
        $this->params = $params;
        $this->tags = $tags;
    }

    /**
     * Execute the job.
     *
     * @return  void
     */
    public function handle()
    {
        $cache = cache();
        if (filled($this->tags)) {
            $cache = \Cache::tags($this->tags);
        }

        if ($cache->has($this->key) || $cache->has('caching_' . $this->key)) {
            return;
        }

        $cache->put('caching_' . $this->key, '1', $this->timeout);
        $cache->put($this->key, \App::call($this->method, $this->params), $this->expire);
        $cache->forget('caching_' . $this->key);
    }
}
