<?php

namespace App\Jobs;

use App\Mail\EmailForQueuing;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 20;

    public $tries = 2;

    protected $response;


    public function __construct($response)
    {
        $this->response = $response;
    }

    public function handle()
    {
        $emailbody = new EmailForQueuing( $this->response);
        Mail::to($this->response['email'])->send($emailbody);
    }
}
