<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSMSJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int $timeout
     */
    public $timeout = 20;

    /**
     * @var int $tries
     */
    public $tries = 2;

    /**
     * @var string $mobile
     */
    protected $mobile;

    /**
     * @var string $message
     */
    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mobile, $message)
    {
        $this->mobile = $mobile;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $response = json_decode($client->request('POST', 'http://RestfulSms.com/api/Token', [
            'json' => [
                'UserApiKey' => '1dec4cbd9dd3fd5e3b905a02',
                'SecretKey' => 'Nima1373@'
            ]
        ])->getBody()->getContents());

        if ($response->IsSuccessful === false) {
            throw new \Exception((string) json_encode($response, JSON_UNESCAPED_UNICODE));
        }

        $response = json_decode($client->request('POST', 'https://RestfulSms.com/api/MessageSend', [
            'json' => [
                'Messages' => [$this->message],
                'MobileNumbers' => [$this->mobile],
                'LineNumber' => '50002015191513',
                'CanContinueInCaseOfError' => 'true',
            ],
            'headers' => ['x-sms-ir-secure-token' => $response->TokenKey]
        ])->getBody()->getContents());




    }
}
