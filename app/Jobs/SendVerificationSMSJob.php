<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendVerificationSMSJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int $timeout
     */
    public $timeout = 20;

    /**
     * @var int $trie
     */
    public $tries = 2;

    /**
     * @var string $mobile
     */
    protected $mobile;

    /**
     * @var string $code
     */
    protected $code;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mobile, $code)
    {
        $this->mobile = $mobile;
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $response = json_decode($client->request('POST', 'http://RestfulSms.com/api/Token', [
            'json' => [
                'UserApiKey' => '1dec4cbd9dd3fd5e3b905a02',
                'SecretKey' => 'Nima1373@'
            ]
        ])->getBody()->getContents());

        if (in_array($response->IsSuccessful, [false, 'false'])) {
            throw new \Exception((string) json_encode($response, JSON_UNESCAPED_UNICODE));
        }

        $response = json_decode($client->request('POST', 'https://RestfulSms.com/api/VerificationCode', [
            'json' => [
                'Code' => $this->code,
                'MobileNumber' => $this->mobile
            ],
            'headers' => ['x-sms-ir-secure-token' => $response->TokenKey]
        ])->getBody()->getContents());

        if (in_array($response->IsSuccessful, [false, 'false'])) {
            throw new \Exception((string) json_encode($response, JSON_UNESCAPED_UNICODE));
        }
    }
}
