<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $fillable = ['title','body','image'];
    protected $casts = ['image'=>'array'];
    public $timestamps = false;
}
