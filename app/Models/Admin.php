<?php

namespace App\Models;

use App\Traits\HasFile;
use App\Traits\HasMeta;
use App\Traits\HasRole;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable,HasMeta;


    protected $guard='admin';


    protected $fillable = [
        'name','email'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];



}
