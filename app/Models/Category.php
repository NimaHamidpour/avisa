<?php

namespace App\Models;

use App\Traits\HasFile;
use App\Traits\HasMeta;
use App\Traits\HasRelation;
use App\Traits\HasScope;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasScope,HasSlug,HasRelation,HasMeta,HasFile;


    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */

    /**
     * HasFile Trait initial data.
     */
    protected $has_file = [
        'name' => 'title',
        'source' => 'icon',

    ];
    protected static $slug_source = 'title';

    /**
     * @param  string $value
     *
     * @return  void
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = \Str::slug($value, ' ', '');
    }


    public function Meta()
    {
        return $this->belongsTo('App\Models\Meta','id','model_id');
    }

    public function Service()
    {
        return $this->hasMany('App\Models\Service','category','id');
    }
}
