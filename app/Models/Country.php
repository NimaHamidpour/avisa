<?php

namespace App\Models;

use App\Traits\HasMeta;
use App\Traits\HasScope;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasScope,HasSlug,HasMeta;


    protected $fillable = ['title','slug','flag','description','body','tags','keyword','writer'];

    protected static $slug_source = 'title';

}
