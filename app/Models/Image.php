<?php

namespace App\Models;

use App\Traits\HasFile;
use App\Traits\HasMeta;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasMeta;

    protected $fillable = ['src','url','title','key'];

    protected $casts = ['src'=>'array'];

    public $timestamps = false;


    public function Parent()
    {
        return $this->belongsTo('App\Models\Setting','key','key');
    }
}
