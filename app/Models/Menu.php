<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['title','url','position','parent'];

    public $timestamps = false;

    public function Child()
    {
        return $this->hasMany('App\Models\Menu','parent','id');
    }
}
