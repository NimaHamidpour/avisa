<?php

namespace App\Models;

use App\Traits\HasMeta;
use App\Traits\HasScope;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasScope,HasSlug,HasMeta;

    protected $casts = ['image'=>'array'];

    protected $fillable = ['title','slug','image','description','body','tags','keyword','type','model','model_id','writer'];

    protected static $slug_source = 'title';

    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function Category()
    {
        return $this->belongsTo('App\Models\Category','model_id','id');
    }

}
