<?php

namespace App\Models;

use App\Traits\HasMeta;
use App\Traits\HasScope;
use App\Traits\HasSlug;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasScope,HasSlug,HasMeta;

    protected $casts = ['image'=>'array'];

    protected $fillable = ['title','slug','image','description','body','tags','keyword','country','category','writer'];

    protected static $slug_source = 'title';

    public function Admin()
    {
        return $this->belongsTo('App\Models\Admin','writer','id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Models\Category','category','id');
    }

    public function Country()
    {
        return $this->belongsTo('App\Models\Country','country','id');
    }
}
