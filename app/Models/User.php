<?php

namespace App\Models;

use App\Traits\HasFile;
use App\Traits\HasMeta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Shetabit\Visitor\Traits\Visitor;

class User extends Authenticatable
{
    use Notifiable,HasFile,HasMeta;

    protected $has_file = [
        'name' => 'name',
        'source' => 'avatar',
        'image' => [
            'fit' => ['150']
        ]
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
    ];
}
