<?php

namespace App\Traits;

trait HasRelation
{
    /**
     * @return  void
     */
    protected static function bootHasRelation()
    {
        static::deleting(function ($model) {
            $model->pivotTruncate();
        });
    }

    /**
     * @return  void
     */
    public function initializeHasRelation()
    {
        if (!in_array('pivot', $this->hidden)) {
            $this->hidden[] = 'pivot';
        }
    }

    /**
     * @param  bool $isSource
     * @param  string $target
     * @param  string|null $type
     *
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function pivotRelation(bool $isSource, string $target, $type = null)
    {
        return $this->belongsToMany(
            'App\Models\\' . $target,
            'relations',
            ($isSource ? 'source' : 'target') . '_id',
            ($isSource ? 'target' : 'source') . '_id'
        )
            ->wherePivot('source_model', ($isSource ? class_basename($this) : $target))
            ->wherePivot('target_model', ($isSource ? $target : class_basename($this)))
            ->wherePivot('type', $type)
            ->withTimestamps();
    }

    /**
     * relationship attach with models values.
     *
     * @param  string $relation
     * @param  int|\Illuminate\Database\Eloquent\Model $target
     * @param  string|null $status
     *
     * @return  void
     */
    public function pivotAttach(string $relation, $target, $status = null)
    {
        if (blank($target)) return;
        $relation = $this->$relation();
        $wheres = ((array) $relation)[chr(0) . '*' . chr(0) . 'pivotWheres'];
        $relation->attach($target, [
            'source_model' => $wheres[0][1],
            'target_model' => $wheres[1][1],
            'type' => $wheres[2][1],
            'status' => $status
        ]);
    }

    /**
     * @return  bool
     */
    public function pivotTruncate()
    {
        return (bool) \App\Models\Relation::where(function ($query) {
            $query->where('source_model', class_basename($this))->where('source_id', $this->id);
        })->orWhere(function ($query) {
            $query->where('target_model', class_basename($this))->where('target_id', $this->id);
        })->delete();
    }
}
