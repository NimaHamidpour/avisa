<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title');
            $table->string('slug')->unique();

            $table->text('image')->nullable();

            $table->longText('body');

            $table->enum('type', ['blog', 'page', 'section'])->default('blog');

            $table->unsignedBigInteger('model_id')->nullable();
            $table->string('model')->nullable();

            $table->unsignedBigInteger('writer')->nullable();

            $table->unsignedInteger('view_count')->default('0');
            $table->unsignedInteger('comment_count')->default('0');

            $table->text('description');
            $table->text('tags')->nullable();
            $table->text('keyword')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
