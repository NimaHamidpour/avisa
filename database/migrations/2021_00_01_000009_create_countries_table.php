<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();

            $table->string('flag');

            $table->string('title');
            $table->string('slug')->unique();

            $table->text('body');

            $table->text('description');
            $table->text('tags')->nullable();
            $table->text('keyword')->nullable();

            $table->unsignedInteger('view_count')->default('0');
            $table->unsignedInteger('comment_count')->default('0');

            $table->unsignedBigInteger('writer')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
