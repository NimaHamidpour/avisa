<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('country');
            $table->unsignedInteger('category');

            $table->string('title');
            $table->string('slug')->unique();

            $table->string('image');

            $table->text('body');

            $table->text('description');
            $table->text('tags')->nullable();
            $table->text('keyword')->nullable();

            $table->unsignedInteger('view_count')->default('0');
            $table->unsignedInteger('comment_count')->default('0');

            $table->foreign('country')->on('countries')->references('id')->onDelete('cascade');
            $table->foreign('category')->on('categories')->references('id')->onDelete('cascade');

            $table->unsignedInteger('writer');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
