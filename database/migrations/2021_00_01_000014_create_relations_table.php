<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relations', function (Blueprint $table) {
            $table->string('source_model');
            $table->unsignedBigInteger('source_id');
            $table->string('target_model');
            $table->unsignedBigInteger('target_id');
            $table->string('type')->nullable(); // for prevent duplicates
            $table->integer('status')->nullable(); // use for status or meta value
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relations');
    }
}
