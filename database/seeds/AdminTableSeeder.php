<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(array(
            array
            (
                'name'=>'نیما حمیدپور',
                'email'=>'hamidpour91.itsu@yahoo.com',
                'password'=>'$2y$10$XDXVre.FMHbeONxwrLlfnOqc8x7hqGDGX/UPeZKLq.eS5CyHdoSbK',
            ),
        ));
    }
}
