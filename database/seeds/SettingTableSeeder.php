<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(array(
            array(
                'key'=>'app_logo',
                'value'=>'storage/setting/logo.png',
                'title'=>'Logo',
                'type'=>'setting',
            ),
            array(
                'key'=>'app_tell',
                'value'=>'09034732125-09011599467',
                'title'=>'website tell',
                'type'=>'setting',
            ),
            array(
                'key'=>'app_name',
                'value'=>'گروه مهاجرتی آویـســا',
                'title'=>'persian website name',
                'type'=>'setting',
            ),
            array(
                'key'=>'app_description',
                'value'=>'گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا گروه مهاجرتی آویـســا',
                'title'=>'website description ',
                'type'=>'setting',
            ),

            array(
                'key'=>'app_slogan',
                'value'=>'آویسا پلی به سوی آینده ای روشن',
                'title'=>'website slogan ',
                'type'=>'setting',
            ),
            array(
                'key'=>'social',
                'value'=>'شبکه های اجتماعی',
                'title'=>'social network',
                'type'=>'setting',
            ),
        ));
    }

}
