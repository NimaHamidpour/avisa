function loadBlog(parent,id)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.blog_group').not(parent).removeClass('active');
    $(parent).toggleClass('active');

    $.ajax({
        url:'/Api/loadBlog',
        method:'post',
        data:{id:id},
        success:function (data) {
            if(data!=='')
            {
                $('#blogs-view').html(data);
                $("#blog-carousel").owlCarousel({
                    margin: 40,
                    loop: false,
                    autoplay: true,
                    items: 4,
                    rtl: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        500: {
                            items: 2
                        },
                        800: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
            }
            else
            {
                $('#blogs-view').html('');
            }
        }
    });
}

function suggest_search_blog() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var search=$('#search_text_id').val();

    if (search.length > 3)
    {
        $.ajax({
            url:'/Api/Blog/search',
            method:'post',
            data:{search:search},
            success:function (data) {
                if (data!=='')
                {
                    $('#aj_search_content').html(data)
                                           .append('<div class="mt-2">' +
                                               '<input type="submit" value="مـشاهده همه نتایج" class="btn mx-auto text-center text-dark font-yekan font-size-14 text-decoration-none" href="search/'+search+'"/> </div>');
                }
                else
                {
                    $('#aj_search_content').html('<div class="alert alert-warning text-center font-size-14">موردی یافت نشد</div>');
                }
            }
        });
    }
    else
    {
        $('#aj_search_content').html('');
    }
}

function suggest_search_blog_mobile() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var search=$('#search_text_mobile_id').val();

    if (search.length > 3)
    {
        $.ajax({
            url:'/Api/Blog/search',
            method:'post',
            data:{search:search},
            success:function (data) {
                if (data!=='')
                {
                    $('#aj_search_content_mobile').html(data).append('<div class="mt-2">' +
                        '<input type="submit" value="مـشاهده همه نتایج" class="btn btn-light text-black form-control mx-auto text-center text-dark font-yekan font-size-14 text-decoration-none" href="search/'+search+'"/> </div>');
                    ;
                }
                else
                {
                    $('#aj_search_content_mobile').html('<div class="alert alert-warning text-center font-size-14">موردی یافت نشد</div>');
                }
            }
        });
    }
    else
    {
        $('#aj_search_content_mobile').html('');
    }
}

function likeComment(id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url:'/Api/Comment/like',
        method:'post',
        data:{comment:id},
        dataType:'json',
        success:function (data)
        {
            if (data['msg'] === 'success')
            {
                document.getElementById('like'+id).innerText=data['count'];
            }
        }
    });
}

function dislikeComment(id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url:'/Api/Comment/dislike',
        method:'post',
        data:{comment:id},
        dataType:'json',
        success:function (data)
        {
            if (data['msg'] === 'success')
            {
                document.getElementById('dislike'+id).innerText=data['count'];
            }
        }
    });
}

