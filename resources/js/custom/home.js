$("#blog-carousel").owlCarousel({
    margin: 40,
    loop: false,
    autoplay: true,
    items: 4,
    rtl:true,
    responsive: {
        0: {
            items: 1
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$("#service-carousel").owlCarousel({
    margin: 40,
    loop: false,
    autoplay: true,
    items: 4,
    rtl:true,
    responsive: {
        0: {
            items: 1
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$("#certificate-carousel").owlCarousel({
    margin: 30,
    loop: false,
    autoplay: true,
    items: 4,
    rtl:true,
    responsive: {
        0: {
            items: 1
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

function showLargeImage(src,title) {
    var modal = document.getElementById("certificate");

    var modalImg    = document.getElementById("certificate-show");
    var captionText = document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = src;
    captionText.innerHTML = title;

    // Get the <span> element that closes the modal
    var close_certificate_btn = document.getElementById("close-certificate");

    // When the user clicks on <span> (x), close the modal
    close_certificate_btn.onclick = function() {
        modal.style.display = "none";
    }
}


var slidercounter = 0;
function aboutUs_slideShow(type,limit) {

    $('#about_img_'+slidercounter).removeClass('d-block').addClass('d-none');
    $('#about_content_'+slidercounter).removeClass('d-block').addClass('d-none');

    if(type==='plus')
    {
        if(slidercounter < limit)
            slidercounter++;
        else
            slidercounter=0;
    }
    else
    {
        if(slidercounter > 0)
            slidercounter--;
        else
            slidercounter=limit;
    }

    $('#about_img_'+slidercounter).removeClass('d-none').addClass('d-block');
    $('#about_content_'+slidercounter).removeClass('d-none').addClass('d-block');
}

jQuery(document).ready(function ($) {$("span.counter").counterUp({ delay: 100,time: 6000,offset: 100});});
