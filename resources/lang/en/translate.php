<?php
return
    [
        'MainPage'=>'Main Page',
        'EvaluationForm'=>'Counseling Evaluation Form',
        'Certificate'=>'Certificate',
        'CoWorker'=>'Our Partners',
        'Blog'=>'Blog',
        'AboutUs'=>'About Us',
        'ContactUs'=>'Contact Us',
        'Why'=>'Why',
        'Service'=>'Our Services',
        'Counter1'=>'Years of work experience',
        'Counter2'=>'Successful case',
        'Counter3'=>'Experienced consulting staff',
        'Counter4'=>'The case is under investigation',
        'SocialMedia'=>'Follow Us',
        'WorkTime'=>'Hours of work',
        'Address'=>'Address',

        // ******** BLOG ********
        'MostView'=>'Most view',
        'RelatedBlog'=>'Related Article',
        'Search'=>'Search',


        // ******** FORM ********
        'Name'=>'Full name',
        'Mobile'=>'Cellphone',
        'Email'=>'Email',
        'City'=>'City of residency',
        'Birthday'=>'Birthday',
        'Job'=>'Job Title',
        'Experience'=>'Job Experience',
        'MaritalStatus'=>'Marital Status',
        'Child'=>'Number of children',
        'Degree'=>'Latest qualification',
        'Field'=>'Field of study',
        'Sex'=>'Gender',
        'Year'=>'Year',
        'Male'=>'Male',
        'Female'=>'Female',
        'Married'=>'Married',
        'Single'=>'Single',
        'Resume'=>'Resume',
        'Submit'=>'Confirmation'
    ];
