<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon"  type="image/jpg" sizes="96x96" href="{{url((isset($response['app_logo']) ? $response['app_logo'] : 'front/img/structure/websazandeh.png'))}}">

    <title>پـنل مدیریت {{$response['app_name']}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="{{url('/css/app2.css?v=2')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">
</head>
<body class="direction-rtl">

    @include('admin.top.navbar')

    <div id="wrapper">

        @include('admin.side.sidebar')

        <!-- Main -->
            <div id="content-wrapper">
                <div class="container-fluid">

                    <!-- Breadcrumbs-->
                    @include('breadcrumb.breadcrumb')
                    <!-- Breadcrumbs-->

                    <div id="maindiv">
                        @yield('content')
                    </div>

                </div>
            </div>
         <!-- Main -->

    </div>

    <!-- Scroll to Top Buton-->
    <a class="scroll-to-top rounded" href="#wrapper">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="direction:rtl!important;text-align:right;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">

                    <div class="col-11 text-right modal-title" id="exampleModalLabel">از صفحه خارج می شوید؟</div>
                    <div class="col-12 text-left">
                        <button class="close shadow-none" type="button" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>

                </div>
                <div class="modal-body">
                    اگر مطمئن هستید که می خواهید صفحه را ترک کنید روی گزینه خروج کلیک کنید در غیر این صورت گزینه انصراف را
                    فشار دهید.
                </div>
                <div class="modal-footer">
                    <form action="{{route('admin.logout')}}" method="get">
                        @csrf
                        <input type="submit" class="btn btn-danger bold font-size-14" value="خروج">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Logout Modal-->

    <script src="{{url('/JS/app2.js?v=2')}}"></script>
    @yield('script')
    <script>

        // Collapse/Expand icon
        $('#collapse-icon').addClass('fa-angle-double-left');

        // Collapse click
        $('[data-toggle=sidebar-colapse]').click(function() {
            SidebarCollapse();
        });

        function SidebarCollapse () {
            $('#admin-sidebar').toggleClass('d-none');

        }
    </script>
</body>
</html>
