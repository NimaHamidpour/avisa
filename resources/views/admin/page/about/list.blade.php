@extends('admin.index')


@section('content')

    <div class="table-responsive admin-div-table">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ایکون</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">متن</td>
                <td class="font-size-13">تنطیمات</td>
            </tr>
            <tbody>
                    @foreach($response['aboutUs'] as $aboutUs)
                        <tr class="text-center">
                            <td ><img class="admin-table-img rounded" src="{{url($aboutUs->image['400'])}}"></td>
                            <td class="admin-info pt-3"> {{$aboutUs->title}}</td>
                            <td class="admin-black pt-3"> {{mb_substr(strip_tags($aboutUs->body),0,100,'UTF8').'...'}}</td>
                            <td>
                                <a href="{{route('admin.aboutus.destroy',$aboutUs->id)}}"
                                   class="btn btn-outline-danger btn-sm admin-a mt-2">
                                    حذف
                                </a>
                            </td>
                        </tr>
                    @endforeach
            </tbody>
        </table>
    </div>

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form  action="{{route('admin.aboutus.store')}}" method="POST" enctype="multipart/form-data"
                   class="form admin-form">
                @csrf

                <div class="form-row mb-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="image">
                                <span class="admin-input-label d-block">انتخاب ایکــون</span>
                                <img src="{{url('/app/img/image-add-button.png')}}" id="slideshowfileimg"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="image" id="image" accept="image/jpeg" onchange="readURL(this);"
                                   class="d-none">

                            <div class="d-inline-block" id="preview-img"></div>
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-12 mb-4">
                        <label for="title_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;  عنوان </label>
                        <input type="text" class="form-control admin-input"  autocomplete="off"
                               id="title_id" name="title" placeholder="عنوان" value="{{old('title')}}">
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="value" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i> محتوا </label>
                        <textarea class="form-control admin-input" style="height: 120px" name="body" id="value" placeholder="محتوا">{{old('body')}}</textarea>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="اضافه کردن" >
                </div>

            </form>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
