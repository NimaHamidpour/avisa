@extends('admin.index')

@section('content')
    @include('message.message')

    <div class="container">

        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                <form action="{{route('admin.user.store')}}" method="POST" class="form admin-form">
                    @csrf
                    <div class="form-row">

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto my-2">
                            <label for="name-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center user-input rounded-0 @error('name') is-invalid @enderror"
                                   id="name-id" name="name" placeholder="نام و فامیل" value="{{old('name')}}">
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="codemeli-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کد ملی </label>
                            <input type="text"
                                   class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('codemeli') is-invalid @enderror"
                                   id="codemeli-id" name="codemeli" placeholder="1271787202"
                                   value="{{old('codemeli')}}">
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="sex-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;جنسیت</label>
                            <select class="form-control user-input text-option-center" name="sex" id="sex-id">
                                <option value="male">آقا</option>
                                <option value="female">خانم</option>
                            </select>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="mobile-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;تاریخ تولد</label>
                            <input type="text" class="admin-input form-control text-center px-3" id="birthday"
                                   value="انتخاب تاریخ">
                            <input type="text" class="d-none" id="birthday_hidden" value="" name="birthday"/>
                        </div>


                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="mobile-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;شماره تلفن همراه</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('mobile') is-invalid @enderror"
                                   id="mobile-id" name="mobile" placeholder="۰۹ ..." value="{{old('mobile')}}">
                        </div>


                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="tell-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;شماره تلفن ثابت</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('tell') is-invalid @enderror"
                                   id="tell-id" name="tell" placeholder="031 ..." value="{{old('tell')}}">
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="email-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;ایمیل</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center font-family-iransans text-left direction-ltr rounded-0 @error('email') is-invalid @enderror"
                                   id="email-id" name="email" placeholder="info@forex.com" value="{{old('email')}}">
                        </div>


                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="address-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;آدرس</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center admin-input rounded-0 @error('address') is-invalid @enderror"
                                   id="address-id" name="address" placeholder="بهارستان ..."
                                   value="{{old('address')}}">
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="degree-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;آخرین مدرک تحصیلی</label>
                            <select class="form-control user-input text-option-center" name="degree" id="degree-id">
                                <option value="student">دانش آموز</option>
                                <option value="diploma">دیپلم</option>
                                <option value="illiterate">فوق دیپلم</option>
                                <option value="bachelor">لیسانس</option>
                                <option value="master">فوق لیسانس</option>
                                <option value="phd">دکتری</option>
                            </select>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="field-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;رشته تحصیلی</label>
                            <input type="text"
                                   class="form-control font-size-14 text-center admin-input rounded-0 @error('field') is-invalid @enderror"
                                   id="field-id" name="field" placeholder="مهندسی برق - تجربی و..."
                                   value="{{old('field')}}">
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="password-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کلمه عبور</label>
                            <input type="password"
                                   class="form-control font-size-14 text-center font-family-iransans rounded-0 @error('password') is-invalid @enderror"
                                   id="password-id" name="password" placeholder="کلمه عبور"
                                   value="{{old('password')}}">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-2">
                            <label for="password_confirmation-id" class="user-input-label"><i
                                    class="fas fa-pencil-alt prefix text-right"></i>&nbsp;تکرار کلمه عبور</label>
                            <input type="password"
                                   class="form-control font-size-14 text-center font-family-iransans  rounded-0 @error('password_confirmation') is-invalid @enderror"
                                   id="password_confirmation-id" name="password_confirmation"
                                   placeholder="تکرار کلمه عبور" value="{{old('password')}}">
                        </div>
                    </div>


                    <div class="form-row mt-3">
                        <input type="submit"
                               class="col-md-4 btn user-submit form-control mt-3 font-weight-bold font-yekan"
                               value="ثبت نام ">
                    </div>

                </form>
            </div>


        </div>
    </div>


@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#birthday").persianDatepicker({
                altField: '#birthday_hidden',
                altFormat: "X",
                observer: true,
                format: 'YYYY/MM/DD',
                initialValue: false,
                initialValueType: 'persian',
                autoClose: true,
                maxDate: 'today',
            });
        });
    </script>
@endsection
