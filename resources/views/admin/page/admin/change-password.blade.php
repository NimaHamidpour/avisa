@extends('admin.index')

@section('content')

        <div class="container">

            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12 mx-auto text-right">
                    @include('message.message')
                    <form  action="{{route('admin.admin.update.password')}}"
                           method="POST" class="form my-3 admin-form">
                        @method('put')
                        @csrf

                        <div class="form-row">
                            <div class="col-xl-12 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-4">
                                <label for="old_password-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کلمه عبور قبلی</label>
                                <input type="password" class="form-control font-family-iransans font-size-14 @error('old_password') is-invalid @enderror" id="old_password-id" name="old_password" placeholder="کلمه عبور قبلی">
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-4">
                                <label for="password-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;کلمه عبور جدید</label>
                                <input type="password" class="form-control font-family-iransans  font-size-14 @error('password') is-invalid @enderror" id="password-id" name="password" placeholder="کلمه عبور">
                            </div>
                            <div class="col-xl-12 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  mb-4">
                                <label for="password_confirmation-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;تکرار کلمه عبور جدید</label>
                                <input type="password" class="form-control font-family-iransans font-size-14 @error('password_confirmation') is-invalid @enderror" id="password_confirmation-id" name="password_confirmation" placeholder="تکرار کلمه عبور">
                            </div>
                        </div>

                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="ویرایش">
                        </div>

                    </form>
                </div>
            </div>

        </div>

@endsection


@section('script')
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
