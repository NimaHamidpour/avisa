@extends('admin.index')

@section('content')
    @include('message.message')


    <!-- PERSONAL INFO -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('read_student'))
        <div class="row text-right">
            <div class="col-12 text-right">
                <h2 class="font-family-yekan font-size-17 font-weight-bold text-danger mx-3">مشخصات فردی</h2>
            </div>
        </div>
        <div class="table-responsive admin-div-table mt-1">
            <table class="table table-striped">
                <tr class="admin-table-hr-row">
                    <td class="font-size-13">تصویر</td>
                    <td class="font-size-13">نام</td>
                    <td class="font-size-13">ایمیل</td>
                    <td class="font-size-13">موبایل/تلفن</td>
                    <td class="font-size-13">کد ملی</td>
                    <td class="font-size-13">آدرس</td>

                </tr>
                <tbody>
                <tr class="text-center">
                    <td>
                        <img
                            src="{{$response['user']->avatar !='' ? url($response['user']->avatar) : url('/app/img/avatar.jpg')}}"
                            class="admin-table-img rounded-circle">
                    </td>
                    <td class="admin-black">{{$response['user']->name}}</td>
                    <td class="admin-danger">{{$response['user']->email}}</td>
                    <td>
                        <span class="admin-info d-block">{{$response['user']->mobile}}</span>
                        <span class="admin-danger">{{$response['user']->tell}}</span>
                    </td>
                    <td class="admin-success">{{$response['user']->codemeli}}</td>
                    <td class="admin-info">{{$response['user']->address}}</td>

                </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive admin-div-table mt-2">
            <table class="table table-striped">
                <tr class="admin-table-hr-row">
                    <td class="font-size-13">مدرک تحصیلی</td>
                    <td class="font-size-13">رشته تحصیلی</td>
                    <td class="font-size-13">تاریخ ثبت نام</td>
                    <td class="font-size-13">تاریخ تولد</td>

                </tr>
                <tbody>
                <tr class="text-center">

                    <td class="admin-info">
                        @if($response['user']->degree === 'student')
                            دانش آموز
                        @elseif($response['user']->degree === 'diploma')
                            دیپلم
                        @elseif($response['user']->degree === 'illiterate')
                            فوق دیپلم
                        @elseif($response['user']->degree === 'bachelor')
                            لیسانس
                        @elseif($response['user']->degree === 'master')
                            فوق لیسانس
                        @elseif($response['user']->degree === 'phd')
                            دکتری
                        @endif
                    </td>
                    <td class="admin-danger">{{$response['user']->field}}</td>
                    <td class="admin-success">{{jdate($response['user']->created_at)->format('Y/m/d')}}</td>
                    <td class="admin-info">{{jdate($response['user']->birthday)->format('Y/m/d')}}</td>


                </tr>
                </tbody>
            </table>
        </div>
    @endif


    <!-- TERM -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('read_register'))
        <div class="row text-right">
            <div class="col-12 text-right mt-5">
                <h2 class="font-family-yekan font-size-17 font-weight-bold text-danger mx-3">لیست دوره ها</h2>
            </div>
        </div>
        <div class="table-responsive user-div-table mt-1">
            <table class="table table-striped ">
                <tr class="user-table-hr-row">
                    <th class="user-table-hr">کد دوره</td>
                    <th class="user-table-hr">عنوان دوره</td>
                    <th class="user-table-hr">تاریخ شروع</td>
                    <th class="user-table-hr">زمان برگزاری</td>
                    <th class="user-table-hr">وضعیت ثبت نام</td>
                    <th class="user-table-hr">اطلاعات پرداخت</td>

                </tr>
                <tbody>
                @foreach($response['registers'] as $register)
                    <tr class="text-center font-size-13">
                        <td class="user-black">{{$register->term}}</td>
                        <td class="user-black">{{$register->Term->Course->title}}</td>
                        <td class="user-info">{{jdate($register->Term->start_date)->format('Y/m/d')}}</td>
                        <td class="user-black">{{$register->Term->class_time}}</td>
                        <td>
                            @if($register->register_status === 'register')
                                <span class="user-success">ثبت نام</span>
                            @elseif($register->register_status === 'reserve')
                                <span class="user-info">رزرو</span>
                            @else
                                <span class="user-danger">انصرافی</span>
                            @endif
                        </td>
                        <td>
                                <span class="user-info d-block">
                                    {{$register->pay_way === 'cache' ? 'نقدی':'قسطی'}}
                                </span>
                            @if($register->pay_status === 'paid')
                                <span class="user-success">تسویه حساب</span>
                            @else
                                <span class="user-danger">بدهکار</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif


    <!-- PAY -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('read_pay'))
        <div class="row text-right">
            <div class="col-12 text-right mt-5">
                <h2 class="font-family-yekan font-size-17 font-weight-bold text-danger mx-3">وضعیت پرداختی ها</h2>
            </div>
        </div>
        <div class="table-responsive user-div-table mt-1">
            <table class="table table-striped ">
                <tr class="user-table-hr-row">
                    <th class="user-table-hr">کد دوره</td>
                    <th class="user-table-hr">کد پرداختی</td>
                    <th class="user-table-hr">سر رسید</td>
                    <th class="user-table-hr">نحوه پرداخت</td>
                    <th class="user-table-hr">وضعیت پرداخت</td>
                    <th class="user-table-hr">ویرایش</td>
                    <th class="user-table-hr">حذف</td>

                </tr>
                <tbody>
                @foreach($response['pays'] as $pay)
                    <tr class="text-center font-size-13">
                        <td>
                            <span class="admin-info d-block">{{$pay->Register->term}}</span>
                            <span class="admin-success">{{$pay->Register->Term->Course->title}}</span>
                        </td>
                        <td class="user-black">{{$pay->id}}</td>
                        <td class="user-info">
                            {{$pay->pay_date !== null ? jdate($pay->pay_date)->format('Y/m/d') :'نامشخص'}}
                        </td>
                        <td class="user-info">
                            @if($pay->way === 'Online')
                                پرداخت اینترنتی
                            @elseif($pay->way === 'Check')
                                چک
                            @elseif($pay->way === 'Cache')
                                پرداخت حضوری
                            @else
                                تعیین نشده
                            @endif
                        </td>
                        <td>
                            @if($pay->paid === 'yes')
                                <span class="user-success">پرداخت شده</span>
                            @else
                                <span class="user-danger">پرداخت نشده</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('admin.incomepay.edit',$pay->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                        </td>
                        <td>
                            @if($pay->paid === 'yes')
                                <button class="btn btn-secondary btn-sm" disabled>حذف</button>
                            @else
                                <a href="{{route('admin.incomepay.destroy',$pay->id)}}"
                                   class="btn btn-outline-danger btn-sm admin-a">
                                    حذف
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

    <!-- TICKET -->
    @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('read_ticket'))
        <div class="row text-right">
            <div class="col-12 text-right mt-5">
                <h2 class="font-family-yekan font-size-17 font-weight-bold text-danger mx-3">پیام ها</h2>
            </div>
        </div>
        <div class="table-responsive admin-div-table mt-1">
            <table class="table table-striped ">
                <tr class="admin-table-hr-row">
                    <td class="font-size-13"> شماره تیکت</td>
                    <td class="font-size-13">دپارتمان</td>
                    <td class="font-size-13">نام دانش پذیر</td>
                    <td class="font-size-13">موضوع پیام</td>
                    <td class="font-size-13">الویت</td>
                    <td class="font-size-13">وضعیت</td>
                    <td class="font-size-13">تاریخ</td>
                    <td class="font-size-13">مشاهده</td>
                </tr>
                <tbody>
                @foreach($response['tickets'] as $ticket)
                    <tr class="text-center font-size-13">
                        <td class="admin-black direction-ltr">{{$ticket->id}}</td>
                        <td class="admin-success">{{$ticket->Department->title}}</td>
                        <td class="admin-info">{{$ticket->User->name}}</td>

                        <td class="admin-black">{{$ticket->title}}</td>

                        <td class="admin-danger">
                            @if($ticket->priority === 'low')
                                پایین
                            @elseif($ticket->priority === 'medium')
                                متوسط
                            @else
                                زیاد
                            @endif
                        </td>
                        <td>
                            @if($ticket->status==='admin-answer')
                                <span class="admin-info">در انتظار پاسخ کارشناس پشتیبانی</span>
                            @elseif($ticket->status==='admin-answer')
                                <span class="admin-danger">در انتظار پاسخ دانش پذیر</span>
                            @else
                                <span class="admin-success">پایان یافته</span>
                            @endif

                        </td>
                        <td class="admin-info">{{jdate($ticket->created_at)->format('Y/m/d')}}</td>
                        <td>
                            <a href="{{route('admin.ticket.show',$ticket->id)}}" class="btn btn-info pt-1 px-2 btn-sm">
                                مشاهده
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$response['tickets']->links()}}
        </div>
    @endif


@endsection
