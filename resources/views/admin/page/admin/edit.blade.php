@extends('admin.index')

@section('content')

        <div class="container">

            <div class="row">
                <div class="col-12 mx-auto text-right">
                    @include('message.message')
                    <form  action="{{route('admin.admin.update',$response['admin']->id)}}" enctype="multipart/form-data"
                           method="POST" class="form my-3 admin-form">
                        @method('put')
                        @csrf


                        <div class="form-row mt-3">
                            <div class="col-md-6 mx-auto mb-4">
                                <label for="name-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbspنام</label>
                                <input type="text" class="form-control text-right user-input @error('name') is-invalid @enderror" id="name-id" name="name" placeholder="نام و فامیل" value="{{$response['admin']->name}}">
                            </div>
                            <div class="col-md-6 mx-auto mb-4">
                                <label for="email-id" class="user-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;ایمیل</label>
                                <input type="text" class="form-control font-family-iransans font-size-14 text-right direction-ltr @error('email') is-invalid @enderror" id="email-id" name="email"  placeholder="info@forex.com" value="{{$response['admin']->email}}">
                            </div>
                        </div>

                        <div class="form-row mt-5">
                            <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="ویرایش">
                        </div>

                    </form>
                </div>
            </div>

        </div>

@endsection


@section('script')
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
