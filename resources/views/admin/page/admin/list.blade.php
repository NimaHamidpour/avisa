@extends('admin.index')

@section('content')
    @include('message.message')

    <!-- SEARCH -->
    <section>
        <div class="row">
            <div class="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-xs-12 mx-auto">
                <form class="form pb-4 border-bottom direction-rtl" method="post" action="{{route('admin.user.search')}}">
                   @csrf

                    <div class="form-row">

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 text-right">
                            <input class="form-control font-family-yekan font-size-14" name="name" placeholder="نام " value="{{isset($response['user_name']) ? $response['user_name'] :''}}">
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 text-right">
                            <input class="form-control font-family-iransans font-size-14 " name="mobile" placeholder="موبایل" value="{{isset($response['user_mobile']) ? $response['user_mobile'] :''}}">
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 text-right">
                            <select class="form-control user-input" name="sex" id="sex">
                                <option value="" hidden>جنسیت</option>
                                <option value="all">هر دو</option>
                                <option value="male" {{isset($response['user_sex']) && $response['user_sex']==='male'?'selected':''}}>آقا</option>
                                <option value="female" {{isset($response['user_sex']) && $response['user_sex']==='female'?'selected':''}}>خانم</option>
                            </select>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 text-right">
                            <select class="form-control user-input" name="degree" id="degree">
                                <option value="" hidden>مدرک تحصیلی</option>
                                <option value="all">همه </option>
                                <option value="student" {{isset($response['user_degree']) && $response['user_degree']==='student'?'selected':''}}>دانش آموز</option>
                                <option value="diploma" {{isset($response['user_degree']) && $response['user_degree']==='diploma'?'selected':''}}>دیپلم</option>
                                <option value="illiterate" {{isset($response['user_degree']) && $response['user_degree']==='illiterate'?'selected':''}}>فوق دیپلم</option>
                                <option value="bachelor" {{isset($response['user_degree']) && $response['user_degree']==='female'?'bachelor':''}}>لیسانس</option>
                                <option value="master" {{isset($response['user_degree']) && $response['user_degree']==='female'?'master':''}}>فوق لیسانس</option>
                                <option value="phd" {{isset($response['user_degree']) && $response['user_degree']==='female'?'phd':''}}>دکتری</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row mt-3">
                        <input type="submit" class="btn col-xl-4 col-lg-4 col-md-8 col-sm-12 col-xs-12 admin-submit" value="جستجو" >
                    </div>
                </form>
            </div>
        </div>
    </section>


    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">تصویر</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">ایمیل/موبایل</td>
                <td class="font-size-13">تاریخ ثبت نام</td>
                <td class="font-size-13">جزئیات</td>

                @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('edit_student'))
                    <td class="font-size-13">ویرایش اطلاعات</td>
                @endif
            </tr>
            <tbody>
                @foreach($response['users'] as $user)
                    <tr class="text-center">
                        <td>
                            <img src="{{$user->avatar !='' ? url($user->avatar) : url('/app/img/avatar.jpg')}}" class="admin-table-img rounded-circle">
                        </td>
                        <td class="admin-black">{{$user->name}}</td>
                        <td>
                            <span class="admin-info d-block">{{$user->email}}</span>
                            <span class="admin-danger">{{$user->mobile}}</span>
                        </td>
                        <td class="admin-success">{{jdate($user->created_at)->format('Y/m/d')}}</td>

                        <td>
                            <a class="btn btn-outline-success text-decoration-none font-size-12 font-weight-bold"
                               href="{{route('admin.user.show',$user->id)}}">
                                جزئیات
                            </a>
                        </td>

                        @if(\Illuminate\Support\Facades\Auth::user()->hasPermission('edit_student'))
                            <td>
                                <a class="btn btn-outline-info text-decoration-none font-size-12 font-weight-bold"
                                   href="{{route('admin.user.edit',$user->id)}}">
                                    ویرایش
                                </a>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['users']->links()}}
    </div>


@endsection
