@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.blog.list') }}
@endsection

@section('content')

    @include('message.message')

    <div class="row text-right">
        <div class="col-12 px-4">
            <a class="text-decoration-none btn btn-info text-right
                        btn-sm font-yekan font-size-14"
               href="{{route('admin.blog.create')}}">
                افزودن بلاگ جدید
            </a>
        </div>
    </div>

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">دسته</td>
                <td class="font-size-13">تعداد بازدید</td>
                <td class="font-size-13">تعداد کامنت</td>
                <td class="font-size-13">تنظیمات</td>
            </tr>
            <tbody>
            @foreach($response['pages'] as $row => $page)

                <tr class="text-center font-size-13">
                    <td class="admin-danger">{{++$row}}</td>
                    <td class="admin-info">{{$page->title}}</td>
                    <td class="admin-success">{{$page->type ==='blog' ? optional($page->Category)->title : optional($page->Category)->title}}</td>
                    <td class="admin-info">{{$page->view_count}}</td>
                    <td class="admin-danger">{{$page->comment_count}}</td>
                    <td>
                        <a href="{{route('app.blog.show',$page->slug)}}" class="btn btn-outline-success btn-sm admin-a">
                            مشاهده
                        </a>
                        <a href="{{route('admin.blog.edit',$page->slug)}}" class="btn btn-outline-primary btn-sm admin-a">
                            ویرایش
                        </a>
                        <a href="{{route('admin.blog.destroy',$page->slug)}}"
                           class="btn btn-outline-danger btn-sm admin-a">
                            حذف
                        </a>

                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['pages']->links()}}
    </div>

@endsection
