@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.category.edit',$response['category']) }}
@endsection

@section('content')
    <section class=" direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.category.update',$response['category']->id)}}" method="POST"
                  enctype="multipart/form-data"
                  class="form admin-form">
                @csrf

                <div class="form-row mb-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="bold d-block">انتخاب ایکــون</span>
                                <img
                                    src="{{$response['category']->icon!='' ? url($response['category']->icon) : url('/app/img/image-add-button.png')}}"
                                    id="slideshowfileimg"
                                    class="img-thumbnail mt-2"
                                    height="100px" width="100px" style="border: 1px dashed #cecece"/>
                            </label>
                            <input type="file" name="icon" id="photo" onchange="readURL(this)"
                                   class="d-none">

                            <div class="d-inline-block" id="preview-img"></div>
                        </div>
                    </div>
                </div>
                <div class="form-row mt-3">

                    <div class="col-md-6 mx-auto mb-4">
                        <label for="title_id" class="font-size-15 bold">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته </label>
                        <input type="text"
                               class="form-control  font-family-iransans @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder="نام دسته"
                               value="{{$response['category']->title}}">
                    </div>

                    <div class="col-md-6 mx-auto mb-4">
                        <label for="type_id" class="font-size-15 bold"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;دسته والد مورد نظر را انتخاب کنید</label>
                        <select id="type_id" name="type" class="form-control text-right font-iran"
                                data-placeholder="دسته والد مورد نظر را انتخاب کنید">
                            <option value="blog" {{$response['category']->type ==='blog' ? 'selected' : ''}}>بلاگ</option>
                            <option value="service" {{$response['category']->type ==='service' ? 'selected' : ''}}>خدمات</option>
                        </select>
                    </div>

                </div>


                <div class="form-row my-4">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit mx-auto "
                           value="ویرایش ">
                </div>
            </form>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
