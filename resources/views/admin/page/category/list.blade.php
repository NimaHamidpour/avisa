@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.category.list',$response['type']) }}
@endsection


@section('content')
    @include('message.message')

    <div class="table-responsive admin-div-table">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">ایکون</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">تنظیمات</td>
            </tr>
            <tbody>
                @foreach($response['categories'] as $row => $category)
                    <tr class="text-center">
                        <td class="admin-danger pt-3">{{++$row}}</td>

                        <td>
                            <img src="{{$category->icon!='' ? url($category->icon) : url('/app/img/structure/image-add-button.png')}}" class="admin-table-img">
                        </td>
                        <td class="admin-black  pt-3">{{$category->title}}</td>
                        <td class="pt-3">
                            <a href="{{route('admin.category.edit',$category->id)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                            <a href="{{route('admin.category.destroy',$category->id)}}" class="btn btn-outline-danger btn-sm admin-a">
                                حذف
                            </a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['categories']->links()}}
    </div>

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">

            <form  action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data"
                   class="form admin-form">
                @csrf

                <div class="form-row mb-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب ایکــون</span>
                                <img src="{{url('/app/img/image-add-button.png')}}" id="photo-img"
                                     class="img-thumbnail admin-form-img p-1"/>
                            </label>
                            <input type="file" name="icon" id="photo" onchange="readURL(this);"
                                   accept="image/x-png,image/gif,image/jpeg" class="d-none">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">

                    <div class="col-md-6 mx-auto mb-4">
                        <label for="title_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;نام دسته </label>
                        <input type="text" autocomplete="off" class="form-control admin-input @error('title') is-invalid @enderror" id="title_id" name="title" placeholder="نام دسته" value="{{old('title')}}">
                    </div>

                    <div class="col-md-6 mx-auto mb-4">
                        <label for="type" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;گروه دسته </label>
                        <select class="form-control admin-input" id="type" name="type">
                            <option class="admin-input" value="blog" {{ $response['type']==='blog' ? 'selected' :''}}>بلاگ</option>
                            <option class="admin-input" value="service" {{ $response['type']==='service' ? 'selected' :''}}>خدمات</option>
                        </select>
                    </div>


                </div>
                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="افزودن" >
                </div>
            </form>
        </div>
    </section>

@endsection
@section('script')
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#photo-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
