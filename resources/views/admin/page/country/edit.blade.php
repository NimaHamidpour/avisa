@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.country.edit',$response['country']) }}
@endsection

@section('content')
    <section class="direction-ltr bg-white pt-0" id="welcome">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.country.update', $response['country']->slug)}}" method="POST"
                  enctype="multipart/form-data"
                  class="form admin-form">
                @csrf
                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="image">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img
                                    src="{{ $response['country']->image !='' ? url( $response['country']->image['400']) : url('/app/img/image-add-button.png')}}"
                                    id="slideshowfileimg"
                                    class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="image" id="image" onchange="readURL(this);"
                                   class="d-none" value="">
                        </div>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-12 mb-4">
                        <label for="title_id" class="admin-input-label bold"><i
                                class="fas fa-pencil-alt prefix text-right"></i> عنوان کشور </label>
                        <input type="text" name="title" id="title_id" value="{{ $response['country']->title}}"
                               class="admin-input form-control" placeholder="عنوان کشور">
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="description" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp; توضیحات کوتاه</label>
                        <textarea class="form-control admin-input p-2" id="description" name="description"
                                  placeholder="توضیحات">{{$response['country']->description}}</textarea>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="body" class="admin-input-label bold"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp; محتوا</label>
                        <textarea class="form-control admin-input p-2 content" id="body" name="body"
                                  placeholder="محتوا">{{ $response['country']->body}}</textarea>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-12 mx-auto mb-4">
                        <label for="keyword" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            کلمات کلیدی
                        </label>
                        <input type="text" class="form-control admin-input" autocomplete="off"
                               id="keyword" name="keyword" value="{{$response['country']->keyword}}"
                               placeholder=" کلمات کلیدی (با enter جدا کنید)"/>

                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-12 mx-auto mb-4">
                        <label for="tags" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;
                            برچسب ها
                        </label>
                        <input type="text" class="form-control admin-input" autocomplete="off"
                               id="tags" name="tags" value="{{$response['country']->tags}}"
                               placeholder="برچسب ها (با enter جدا کنید)"/>

                    </div>
                </div>


                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ویرایش اطلاعات">
                </div>
            </form>
        </div>
    </section>

@endsection


@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.11/tinymce.min.js"></script>
    <script type="text/javascript">

        tiny();

        // **** TINY MCE
        function tiny() {
            var editor_config = {
                path_absolute: "/",
                selector: "textarea.content",
                directionality: 'rtl',
                height: '350px',
                plugins: [
                    "advlist autolink lists  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],//image media link
                toolbar: "insertfile undo redo | styleselect fontselect fontsizeselect forecolor  | bold italic | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent | link",
                fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 22px 24px 36px",
                relative_urls: false,
                file_browser_callback: function (field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file: cmsURL,
                        title: 'Filemanager',
                        width: x * 0.8,
                        height: y * 0.8,
                        resizable: "yes",
                        close_previous: "no"
                    });
                }
            };
            tinymce.init(editor_config);
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }


        var tags = document.querySelector('input[name=tags]');
        var keyword = document.querySelector('input[name=keyword]');

        new Tagify(tags);
        new Tagify(keyword);
    </script>

@endsection
