@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.country.list') }}
@endsection

@section('content')

    @include('message.message')

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">تعداد بازدید</td>
                <td class="font-size-13">تعداد کامنت</td>
                <td class="font-size-13">تنظیمات</td>
            </tr>
            <tbody>
            @foreach($response['country'] as $row => $country)

                <tr class="text-center font-size-13">
                    <td class="admin-danger">{{++$row}}</td>
                    <td class="admin-info">{{$country->title}}</td>
                    <td class="admin-info">{{$country->view_count}}</td>
                    <td class="admin-info">{{$country->comment_count}}</td>
                    <td>
                        <a href="{{route('admin.country.edit',$country->slug)}}"
                           class="btn btn-outline-primary btn-sm admin-a">
                            ویرایش
                        </a>
                        <a href="{{route('admin.country.destroy',$country->slug)}}"
                           class="btn btn-outline-danger btn-sm admin-a">
                            حذف
                        </a>

                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['country']->links()}}
    </div>

@endsection
