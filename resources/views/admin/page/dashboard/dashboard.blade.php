@extends('admin.index')


@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.dashboard') }}
@endsection


@section('content')

    <div class="container">
        <div class="row">

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-3 d-none">
                <a class="decoration-none" href="{{route('admin.user.list')}}">
                    <div class="card text-white admin-bg-dashborad border-none o-hidden h-100">
                        <div class="card-body">

                            <div class="text-center font-weight-bold font-size-15">کاربران</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer font-family-iransans  font-size-20"
                             id="count_waiting_id">

                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.blog.list')}}">
                    <div class="card text-white admin-bg-dashborad border-none o-hidden h-100">
                        <div class="card-body">
                            <div class="text-center font-weight-bold font-size-15">لیست مطالب سایت</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer font-family-iransans  font-size-20"
                             id="count_waiting_id">

                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.service.list')}}">
                    <div class="card text-white admin-bg-dashborad border-none o-hidden h-100">
                        <div class="card-body">
                            <div class="text-center font-weight-bold font-size-15">لیست خدمات</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer font-family-iransans  font-size-20"
                             id="count_waiting_id">

                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.setting.edit')}}">
                    <div class="card text-white admin-bg-dashborad border-none o-hidden h-100">
                        <div class="card-body">
                            <div class="text-center font-weight-bold font-size-15">تنظیمات سایت</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer font-family-iransans  font-size-20"
                             id="count_waiting_id">

                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-3">
                <a class="decoration-none" href="{{route('admin.social.list')}}">
                    <div class="card text-white admin-bg-dashborad border-none o-hidden h-100">
                        <div class="card-body">
                            <div class="text-center font-weight-bold font-size-15">شبکه های اجتماعی</div>
                        </div>
                        <div class="card-footer clearfix z-1 text-center static-answer font-family-iransans  font-size-20"
                             id="count_waiting_id">

                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row px-3">
            <div class="col-12 border border-dark rounded">

                <div class="text-right mt-3">
                    <span class="user-danger font-size-15 font-family-yekan font-weight-bold">بازدید کلی : </span>
                    <span class="user-info font-size-14 font-family-iransans font-weight-bold"> {{ $response['view_all']}}</span>
                </div>
                <div class="post-stats__graph">
                    @if($response['max_view_count'] > 0 )
                        @for($i=10 ;$i >= 0;$i--)
                            <div class="post-stats__bar"
                                 style="height: {{(($response['view_count'][$i] * 100)/$response['max_view_count'])}}px">
                                <div class="post-stats__visits font-family-iransans">{{$response['view_count'][$i]}}</div>
                                <div class="post-stats__title font-family-iransans">{{$response['view_date'][$i]}}</div>
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
