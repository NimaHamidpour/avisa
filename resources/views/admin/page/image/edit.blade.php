@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.image.edit',$response['image']) }}
@endsection


@section('content')

    <section class="bg-white pt-0" id="welcome">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.image.update',$response['image']->id)}}"
                  method="POST" enctype="multipart/form-data"
                  class="form admin-form">
                @csrf


                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="image">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{isset($response['image']->src[$response['image']->Parent->value]) ? url($response['image']->src[$response['image']->Parent->value]): url('/app/img/image-add-button.png') }}" id="preview"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="image" id="image" onchange="readURL(this);"
                                   class="d-none">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="title_id" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            عنوان
                        </label>
                        <input type="text" class="form-control admin-input @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder="عنوان"
                                value="{{$response['image']->title}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="url" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            آدرس انتقال کاربر هنگام کلیک روی تصویر
                        </label>
                        <input type="text" class="form-control admin-input"
                               id="url" name="url" placeholder="آدرس انتقال کاربر هنگام کلیک روی تصویر"
                                value="{{$response['image']->url}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="parent" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            والد
                        </label>
                        <select class="form-control admin-input" name="parent" id="parent">
                            @foreach($response['parent'] as $parent)
                                <option value="{{$parent->key}}"
                                    {{$parent->key === $response['image']->key ? 'selected' : ''}}>
                                    {{$parent->title}}
                                    ({{$parent->value}})
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 btn admin-submit"
                           value="ویرایش کردن">
                </div>
            </form>
        </div>

    </section>


@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
