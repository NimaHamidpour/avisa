@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.image.list') }}
@endsection


@section('content')

    <section class="bg-white pt-0" id="welcome">

        <div class="table-responsive admin-div-table">
            <table class="table table-striped table-bordered">
                <tr class="admin-table-hr-row">
                    <td class="font-size-13">ردیف</td>
                    <td class="font-size-13">تصویر</td>
                    <td class="font-size-13">عنوان</td>
                    <td class="font-size-13">آدرس</td>
                    <td class="font-size-13">بخش</td>
                    <td class="font-size-13">تنظیمات</td>
                </tr>
                <tbody>
                @foreach($response['image'] as $row => $image)
                    <tr class="text-center font-size-14">
                        <td class="admin-danger pt-4">{{++$row}}</td>
                        <td>
                            <img src="{{url($image->src[$image->Parent->value])}}" class="admin-table-img img-thumbnail">
                        </td>
                        <td class="admin-black pt-4">
                            {{$image->title}}
                        </td>
                        <td class="pt-4">
                            <a href="{{$image->url}}" class="text-decoration-none admin-info">
                                {{$image->url}}
                            </a>
                        </td>
                        <td class="admin-success pt-4">
                            {{$image->Parent->title}}
                        </td>
                        <td>
                            <a href="{{route('admin.image.edit',$image->id)}}"
                               class="btn btn-outline-info btn-sm font-yekan mt-2">
                                ویرایش
                            </a>
                            <a href="{{route('admin.image.destroy',$image->id)}}"
                               class="btn btn-outline-danger btn-sm font-yekan mt-2">
                                حذف
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.image.store')}}"
                  method="POST" enctype="multipart/form-data"
                  class="form admin-form">
                @csrf
                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="image">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{url('/app/img/image-add-button.png')}}" id="preview"
                                     class="img-thumbnail mt-2"
                                     height="100px" width="100px" style="border: 1px dashed #cecece"/>
                            </label>

                            <input type="file" name="image" id="image" onchange="readURL(this);"
                                   class="d-none">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="title_id" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            عنوان
                        </label>
                        <input type="text" class="form-control admin-input @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder=" عنوان ">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="url" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            آدرس انتقال کاربر هنگام کلیک روی تصویر
                        </label>
                        <input type="text" class="form-control admin-input"
                               id="url" name="url" placeholder="  آدرس انتقال کاربر هنگام کلیک روی تصویر ">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-4">
                        <label for="parent" class="admin-input-label">
                            <i class="fas fa-pencil-alt prefix text-right"></i>
                            والد
                        </label>
                        <select class="form-control admin-input" name="parent" id="parent">
                            @foreach($response['parent'] as $parent)
                                <option value="{{$parent->key}}">
                                    {{$parent->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-xs-12 btn admin-submit"
                           value="اضافه کردن">
                </div>
            </form>
        </div>

    </section>


@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
