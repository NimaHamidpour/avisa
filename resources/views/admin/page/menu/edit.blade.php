@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.menu.edit',$response['menu']) }}
@endsection


@section('content')
    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            <form  action="{{route('admin.menu.update',$response['menu']->id)}}" method="POST" class="form admin-form">
                @csrf

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="title_id" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;عنوان</label>
                        <input type="text" autocomplete="off" class="form-control admin-input @error('title') is-invalid @enderror"
                               id="title_id" name="title" placeholder="عنوان" value="{{$response['menu']->title}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="url" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;آدرس صفحه ای که کاربر با کلیک منتقل میگردد</label>
                        <input type="text" autocomplete="off" class="form-control admin-input"
                               id="url" name="url" placeholder="آدرس صفحه ای که کاربر با کلیک منتقل میگردد"
                               value="{{$response['menu']->url}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="parent" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;انتخاب والد</label>

                        <select class="form-control admin-input" id="parent" name="parent">
                            <option class="admin-input" value="" {{$response['menu']->parent == '' ? 'selected' : ''}}>سرگروه</option>
                            @foreach($response['parent'] as $parent)
                                <option class="admin-input" value="{{$parent->id}}"
                                    {{$parent->id === $response['menu']->parent ? 'selected' : ''}}>{{$parent->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="position" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;محل قرارگیری</label>
                        <select class="form-control admin-input" id="position" name="position">
                            <option class="admin-input" value="top" {{$response['menu']->position === 'top' ? 'selected' : ''}}>بالا</option>
                            <option class="admin-input" value="bottom" {{$response['menu']->position === 'bottom' ? 'selected' : ''}}>پایین</option>
                            <option class="admin-input" value="both" {{$response['menu']->position === 'both' ? 'selected' : ''}}>بالا و پایین</option>
                        </select>
                    </div>

                </div>
                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit" value="ویرایش اطلاعات" >
                </div>
            </form>
        </div>
    </section>
@endsection


