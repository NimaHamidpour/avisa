@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.menu.list') }}
@endsection


@section('content')
    @include('message.message')

    <div class="table-responsive admin-div-table">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">آدرس</td>
                <td class="font-size-13">والد</td>
                <td class="font-size-13">جایگاه</td>
                <td class="font-size-13">تنظیمات</td>
            </tr>
            <tbody id="menu_table">
            @foreach($response['menu'] as $row => $menu)
                <tr class="text-center">

                    <td class="admin-danger">{{++$row}}</td>

                    <td class="admin-black">{{$menu->title}}</td>

                    <td>
                        @if($menu->url != null)
                            <a class="text-decoration-none admin-secondary" href="{{$menu->url}}">{{$menu->url}}</a>
                        @else
                            <span class="admin-danger">تعـریف نشده</span>
                        @endif
                    </td>

                    <td class="admin-black">{{$menu->parent != null ? optional($menu->Parent)->title : 'سـرگروه'}}</td>

                    <td class="admin-info">
                        @switch($menu->position)
                            @case('top')
                            بالا
                            @break
                            @case('bottom')
                            پایین
                            @break
                            @case('both')
                            بالا و پایین
                            @break
                        @endswitch
                    </td>
                    <td>
                        <a href="{{route('admin.menu.edit',$menu->id)}}"
                           class="btn btn-outline-primary btn-sm admin-a">
                            ویرایش
                        </a>
                        <a href="{{route('admin.menu.destroy',$menu->id)}}"
                           class="btn btn-outline-danger btn-sm admin-a">
                            حذف
                        </a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$response['menu']->links()}}
    </div>

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            <form action="{{route('admin.menu.store')}}" method="POST" class="form admin-form">
                @csrf

                <div class="form-row mt-3">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="title_id" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;عنوان</label>
                        <input type="text" autocomplete="off"
                               class="form-control admin-input @error('title') is-invalid @enderror" id="title_id"
                               name="title" placeholder="عنوان" value="{{old('title')}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="url" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp;آدرس
                            صفحه ای که کاربر با کلیک منتقل میگردد</label>
                        <input type="text" autocomplete="off" class="form-control admin-input" id="url" name="url"
                               placeholder="آدرس صفحه ای که کاربر با کلیک منتقل میگردد" value="{{old('url')}}">
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="parent" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;انتخاب والد</label>

                        <select class="form-control admin-input" id="parent" name="parent">
                            <option class="admin-input" value="">سرگروه</option>
                            @foreach($response['parent'] as $parent)
                                <option class="admin-input" value="{{$parent->id}}">{{$parent->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto mb-4">
                        <label for="position" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;محل قرارگیری</label>
                        <select class="form-control admin-input" id="position" name="position">
                            <option class="admin-input" value="top">بالا</option>
                            <option class="admin-input" value="bottom">پایین</option>
                            <option class="admin-input" value="both">بالا و پایین</option>
                        </select>
                    </div>

                </div>
                <div class="form-row my-4">
                    <input type="submit" class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="افزودن">
                </div>
            </form>
        </div>
    </section>

@endsection


@section('script')
    <script type="text/javascript">
        let data = [
                @foreach($response['menu'] as $row => $menu)
            {
                row: '{{$row}}',
                id: '{{$menu->id}}',
                title: '{{$menu->title}}',
                url: '{{$menu->url}}',
                parent: '{{$menu->parent != null ? optional($menu->Parent)->title : 'سـرگروه'}}'
            },
            @endforeach
        ]

        new DragAndDrop(
            {
                el: document.querySelector('#menu_table'),
                list: data,
                template: (item) => {
                    return  `<tr id="${item.id}" class="text-center menu-item" >
                                <td class="admin-danger">${++item.row}</td>
                                <td class="admin-black">${item.title}</td>
                                <td><a class="text-decoration-none admin-secondary" href="${item.url}">${item.url}</a></td>
                                <td class="admin-black">${item.parent}</td>
                                <td><a href="" class="btn btn-outline-primary btn-sm admin-a"> ویرایش</a></td>
                                <td><a href="" class="btn btn-outline-danger btn-sm admin-a"> حذف</a></td>
                             </tr> `;
                }
            })
    </script>
@endsection
