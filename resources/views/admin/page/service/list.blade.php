@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.service.list') }}
@endsection

@section('content')

    @include('message.message')

    <div class="table-responsive admin-div-table mt-3">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">عنوان</td>
                <td class="font-size-13">کشور</td>
                <td class="font-size-13">دسته بندی</td>
                <td class="font-size-13">تعداد بازدید</td>
                <td class="font-size-13">تعداد کانت</td>
                <td class="font-size-13">تنظیمات</td>
            </tr>
            <tbody>
                @foreach($response['service'] as $row => $service)
                    <tr class="text-center font-size-13">

                        <td class="admin-danger">{{++$row}}</td>
                        <td class="admin-black">{{$service->title}}</td>
                        <td class="admin-info">{{$service->Category->title}}</td>
                        <td class="admin-black">{{$service->Country->title}}</td>
                        <td class="admin-info">{{$service->view_count}}</td>
                        <td class="admin-danger">{{$service->comment_count}}</td>
                        <td>
                            <a href="{{route('admin.service.edit',$service->slug)}}" class="btn btn-outline-primary btn-sm admin-a">
                                ویرایش
                            </a>
                            <a href="{{route('admin.service.destroy',$service->slug)}}" class="btn btn-outline-danger btn-sm admin-a">
                                حذف
                            </a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['service']->links()}}
    </div>

@endsection
