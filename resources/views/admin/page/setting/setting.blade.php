@extends('admin.index')

@section('breadCrumb')
    {{ \DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('admin.setting') }}
@endsection


@section('content')

    <section class="direction-ltr bg-white pt-0">
        <div class="container-fluid my-5">
            @include('message.message')
            <form action="{{route('admin.setting.update')}}" method="POST" enctype="multipart/form-data"
                  class="form admin-form">
            @csrf

                <div class="form-row mb-3 p-3">
                    <div class="align-right mr-2">
                        <div class="image-upload">
                            <label for="photo">
                                <span class="admin-input-label d-block">انتخاب تصویر</span>
                                <img src="{{isset($response['app_logo']) ? url( $response['app_logo']): url('/front/img/structure/image-add-button.png') }}" id="slideshowfileimg"
                                     class="img-thumbnail admin-form-img"/>
                            </label>
                            <input type="file" name="photo" id="photo" onchange="readURL(this);"
                                   class="d-none" value="{{isset($response['app_logo']) ? url( $response['app_logo']) : '' }}">
                        </div>
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-md-12 mx-auto mb-4">
                        <label for="title" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;عنوان مجموعه </label>
                        <input type="text" class="form-control  admin-input @error('title') is-invalid @enderror"
                               id="title" name="title" placeholder="عنوان مجموعه " autocomplete="off"
                               value="{{$response['app_name']!='' ? $response['app_name'] : ''}}">
                    </div>

                </div>

                <div class="form-row mt-3">
                    <div class="col-md-12 mx-auto mb-4">
                        <label for="slogan" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp;شعار مجموعه </label>
                        <input type="text" class="form-control  admin-input @error('slogan') is-invalid @enderror"
                               id="slogan" name="slogan" placeholder="شعار مجموعه " autocomplete="off"
                               value="{{$response['app_slogan']!='' ? $response['app_slogan'] : ''}}">
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="col-md-12 mx-auto mb-4">
                        <label for="tell" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp; تلفن تماس
                            <span class="font-size-14 text-danger"> (با - از هم جدا کنید) </span>
                        </label>
                        <input type="text" class="form-control admin-input @error('tell') is-invalid @enderror"
                               id="tell" name="tell" placeholder="تلفن تماس" autocomplete="off"
                               value="{{$response['app_tell']!='' ? $response['app_tell'] : ''}}">
                    </div>
                </div>

                <div class="form-row mt-3">

                    <div class="col-md-12 mx-auto mb-4">
                        <label for="address" class="admin-input-label"><i
                                class="fas fa-pencil-alt prefix text-right"></i>&nbsp; آدرس </label>
                        <input type="text" class="form-control  admin-input @error('address') is-invalid @enderror"
                               id="address" name="address" placeholder="آدرس " autocomplete="off"
                               value="{{$response['app_address']!='' ? $response['app_address'] : ''}}">
                    </div>
                </div>


                <div class="form-row mt-3">
                    <div class="col-md-12 mx-auto mb-4">
                        <label for="description" class="admin-input-label"><i class="fas fa-pencil-alt prefix text-right"></i>&nbsp; توضیحات فوتر</label>
                        <textarea class="form-control admin-input-label p-2 content" id="description" name="description" placeholder="درباره ما">{{$response['app_description']}}</textarea>
                    </div>

                </div>

                <div class="form-row my-4">
                    <input type="submit"
                           class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-xs-10 btn admin-submit"
                           value="ثبت اطلاعات">
                </div>

            </form>
        </div>
    </section>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.9.11/tinymce.min.js"></script>
    <script>

        tiny();


        // **** TINY MCE
        function tiny() {
            var editor_config = {
                path_absolute: "/",
                selector: "textarea.content",
                directionality: 'rtl',
                height: '350px',
                plugins: [
                    "advlist autolink lists  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime  nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern link"
                ],//image media link
                toolbar: "insertfile undo redo | styleselect fontselect fontsizeselect forecolor  | bold italic | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent | link",
                fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 22px 24px 36px",
                relative_urls: false,
                file_browser_callback: function (field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file: cmsURL,
                        title: 'Filemanager',
                        width: x * 0.8,
                        height: y * 0.8,
                        resizable: "yes",
                        close_previous: "no"
                    });
                }
            };
            tinymce.init(editor_config);
        }



        // **** PRIVIEW IMAGE
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slideshowfileimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection
