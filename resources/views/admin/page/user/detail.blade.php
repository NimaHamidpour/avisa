@extends('admin.index')

@section('content')
    @include('message.message')


    <!-- PERSONAL INFO -->
        <div class="container">
            <div class="col-12 text-center">
                <div class="table-responsive admin-div-table mt-1">
                    <table class="table table-striped table-bordered border">
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">نام</td>
                            <td class="admin-black">{{$response['user']->name}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">موبایل</td>
                            <td class="admin-black">{{$response['user']->mobile}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">ایمیل</td>
                            <td class="admin-black">{{$response['user']->email}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">جنسیت</td>
                            <td class="admin-black">{{$response['user']->sex === 'male' ? 'آقا' : 'خانم'}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">محل اقامت</td>
                            <td class="admin-black">{{$response['user']->city}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">مدرک تحصیلی</td>
                            <td class="admin-black">{{$response['user']->degree}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">رشته تحصیلی</td>
                            <td class="admin-black">{{$response['user']->field}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">وضعیت تاهل</td>
                            <td class="admin-black">{{$response['user']->married === 'yes' ? 'متاهل' : 'مجرد'}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">تعداد فرزندان</td>
                            <td class="admin-black">{{$response['user']->child}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">تولد</td>
                            <td class="admin-black">{{jdate($response['user']->birthday)->format('Y/m/d')}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">عنوان شغلی</td>
                            <td class="admin-black">{{$response['user']->job}}</td>
                        </tr>

                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">سابقه کار (سال)</td>
                            <td class="admin-black">{{$response['user']->experience}}</td>
                        </tr>
                        <tr class="font-size-13-row">
                            <th class="font-size-13 text-admin ">رزومه</td>
                            <td class="admin-black">
                                @if($response['user']->resume !== null)
                                    <a href="{{url($response['user']->resume)}}" class="text-decoration-none admin-info">
                                        دانـلود
                                    </a>
                                @else
                                    <span class="admin-danger">
                                        وجود ندارد
                                    </span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>



@endsection
