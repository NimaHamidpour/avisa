@extends('admin.index')

@section('content')
    @include('message.message')

    <div class="table-responsive admin-div-table mt-2">
        <table class="table table-striped table-bordered">
            <tr class="admin-table-hr-row">
                <td class="font-size-13">ردیف</td>
                <td class="font-size-13">نام</td>
                <td class="font-size-13">موبایل</td>
                <td class="font-size-13">جنسیت</td>
                <td class="font-size-13">تاریخ ثبت نام</td>
                <td class="font-size-13">جزئیات</td>

            </tr>
            <tbody>
                @foreach($response['users'] as $row => $user)
                    <tr class="text-center">

                        <td class="admin-danger">{{++$row}}</td>
                        <td class="admin-black">{{$user->name}}</td>
                        <td class="admin-danger"> {{$user->mobile}} </td>
                        <td class="{{$user->sex === 'male' ? 'admin-info' : 'admin-danger'}}"> {{$user->sex === 'male' ? 'آقا' : 'خانم'}} </td>
                        <td class="admin-success">{{jdate($user->created_at)->format('Y/m/d')}}</td>

                        <td>
                            <a class="btn btn-outline-success text-decoration-none font-size-11 btn-sm font-weight-bold"
                               href="{{route('admin.user.show',$user->id)}}">
                                جزئیات
                            </a>
                        </td>

                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$response['users']->links()}}
    </div>


@endsection
