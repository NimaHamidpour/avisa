<!-- Sidebar -->
<ul class="sidebar navbar-nav p-1 m-0 sidebar-width sidebar-background" id="admin-sidebar">


    <!-- ADMIN INFORMATION -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-angle-down arrow-left mt-3 text-admin-icon "></i>

            <img
                src="{{isset(\Illuminate\Support\Facades\Auth::user()->avatar) && \Illuminate\Support\Facades\Auth::user()->avatar !=''? url(\Illuminate\Support\Facades\Auth::user()->avatar) : url('app/img/avatar.jpg')}}"
                style="height: 40px"
                class="rounded-circle mx-2">

            <span class="font-family-yekan font-size-12 text-black font-weight-bold">
                     {{\Illuminate\Support\Facades\Auth::user()->name }}
            </span>

        </a>
        <div class="dropdown-menu p-0 m-0 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.admin.edit')}}" class="nav-link text-right text-black">
                <i class="fas fa-user text-admin-icon "></i>
                <span class="font-family-yekan font-size-12 text-black">ویرایش مشخصات</span>
            </a>

            <a href="{{route('admin.admin.edit.password')}}" class="nav-link text-right text-black">
                <i class="fas fa-lock text-admin-icon "></i>
                <span class="font-family-yekan font-size-12">تغییر کلمه عبور</span>
            </a>
        </div>
    </li>


    <!-- DASHBOARD -->
    <li class="nav-item border-bottom">
        <a class="nav-link text-right text-black menu-padding sidebar-width" href="{{route('admin.dashboard')}}">
            <i class="fas fa-home text-admin-icon"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">داشبورد</span>
        </a>
    </li>


    <!-- USER -->
    <li class="nav-item border-bottom d-none">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user text-admin-icon"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">
                   کاربران
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon "></i>
            </span>

        </a>

        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

            <a href="{{route('admin.user.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-layer-group text-admin-icon "></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">لیست کاربران </span>
            </a>

        </div>
    </li>


    <!-- SERVICE -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-store text-admin-icon "></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">
                خدمات
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon "></i>
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.service.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-list text-admin-icon "></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">لیست خدمات</span>
            </a>
            <a href="{{route('admin.service.create')}}" class="nav-link text-right text-black">
                <i class="fa fa-plus text-admin-icon "></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">افزودن خدمت جدید</span>
            </a>
            <a href="{{route('admin.category.list','service')}}" class="nav-link text-right text-black">
                <i class="fa fa-layer-group text-admin-icon "></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">دسته بندی خدمات</span>
            </a>
        </div>
    </li>


    <!-- COUNTRY -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-map text-admin-icon "></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">
                کشورها
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon "></i>
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.country.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-list text-admin-icon "></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">لیست کشورها</span>
            </a>
            <a href="{{route('admin.country.create')}}" class="nav-link text-right text-black">
                <i class="fa fa-plus text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">افزودن کشور جدید</span>
            </a>
        </div>
    </li>

    <!-- PAGE -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fab fa-chrome text-admin-icon"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold text-black">
                مدیریت صفحات
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon"></i>
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">

            <a href="{{route('admin.page.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-list text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">لیست صفحات</span>
            </a>

            <a href="{{route('admin.page.create')}}" class="nav-link text-right text-black">
                <i class="fa fa-plus text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">افزودن صفحه جدید</span>
            </a>

        </div>
    </li>

    <!-- BLOG -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-blog text-admin-icon"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">
                بلاگ
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon"></i>
            </span>
        </a>
        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.blog.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-list text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">لیست بلاگ</span>
            </a>
            <a href="{{route('admin.blog.create')}}" class="nav-link text-right text-black">
                <i class="fa fa-plus text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">افزودن بلاگ جدید</span>
            </a>

            <a href="{{route('admin.category.list','blog')}}" class="nav-link text-right text-black">
                <i class="fa fa-layer-group text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan font-weight-bold">افزودن دسته جدید</span>
            </a>

        </div>
    </li>


    <!-- SETTING -->
    <li class="nav-item border-bottom pb-1">
        <a class="nav-link text-right text-black menu-padding sidebar-width position-relative" href="#"
           id="pagesDropdown" role="button"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cog text-admin-icon"></i>
            <span class="font-family-yekan font-size-12 font-weight-bold">
                تنظیمات
                <i class="fas fa-angle-down arrow-left mt-2 text-admin-icon"></i>
            </span>
        </a>

        <div class="dropdown-menu p-0 m-1 position-static" aria-labelledby="pagesDropdown">
            <a href="{{route('admin.menu.list')}}" class="nav-link text-right text-black w-100">
                <i class="fas fa-bars text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan d-inline-block font-weight-bold">مدیریت منوها </span>
            </a>
            <a href="{{route('admin.aboutus.list')}}" class="nav-link text-right text-black">
                <i class="fas fa-gem text-admin-icon"></i>
                <span class="font-size-12 font-family-yekan font-weight-bold">درباره ما</span>
            </a>
            <a href="{{route('admin.image.list')}}" class="nav-link text-right text-black w-100">
                <i class="fas fa-image text-admin-icon"></i>
                <span class="text-black font-size-12 font-family-yekan d-inline-block font-weight-bold">مدیریت تصاویر </span>
            </a>
            <a href="{{route('admin.setting.edit')}}" class="nav-link text-right text-black">
                <i class="fa fa-mobile text-admin-icon"></i>
                <span class="font-size-12 font-family-yekan font-weight-bold">تنظیمات</span>
            </a>

            <a class="nav-link text-right text-black" href="{{route('admin.social.list')}}">
                <i class="fab fa-instagram  text-admin-icon"></i>
                <span class="font-family-yekan font-size-12 font-weight-bold">شبکه های اجتماعی</span>
            </a>
        </div>
    </li>
</ul>

