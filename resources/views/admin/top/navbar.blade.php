<nav class="navbar navbar-expand navbar-dark static-top direction-rtl admin-navbar-background">

    <a class="navbar-brand mr-1 font-size-19 font-family-aviny" href="{{Request::root()}}">
        {{$response['app_name']}}
    </a>&nbsp;&nbsp;

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0 mt-2"  data-toggle="sidebar-colapse" id="sidebarToggle">
        <i class="fas fa-bars"></i>
    </button>


    <!-- Navbar -->
    <ul class="navbar-nav mr-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-sign-out-alt text-white"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="userDropdown">
                <a class="dropdown-item text-center" href="#" data-toggle="modal" data-target="#logoutModal">خروج</a>
            </div>
        </li>
    </ul>

</nav>
<div id="mainNav"></div>
