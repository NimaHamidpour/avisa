<section id="footer" class="direction-rtl">
    <div class="container-fluid bg-footer">
        <div class="row row-footer">
            <div class="col-md-12  repeatable">
                <div class="container">

                    <div class="row mt-5">

                        <div class="col-md-6">
                            <p class="font-family-yekan font-size-15 text-right text-gold-grade font-weight-bold">
                                {{$response['app_name']}}
                            </p>

                            <div class="text-justify text-white line-35 font-size-13 direction-rtl" id="txt-footer">
                                {!! $response['app_description'] !!}
                            </div>
                        </div>

                        <div class="col-md-3">
                            <ul class="list-unstyled text-center p-0 direction-ltr mx-auto">
                                <li class="font-family-yekan font-size-13  text-black font-weight-bold mb-4 bg-bt-gold border-radius-10 p-2">
                                    تماس با ما
                                </li>
                                @foreach(explode ("-", $response['app_tell']) as $tell)
                                    <li class="mt-1">
                                        <a class="text-white decoration-none font-size-21 font-family-aviny align-right"
                                           href="tel:{{$tell}}">
                                            &nbsp; {{$tell}}
                                            <i class="fas fa-phone-square text-gold-grade font-size-14"></i>

                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <ul class="list-unstyled text-center p-0">
                                <li class="font-family-yekan font-size-13  text-black font-weight-bold mb-4 bg-bt-gold border-radius-10 p-2">
                                    ما را در شبکه های اجتماعی دنبال کنید
                                </li>
                                @foreach($response['socials'] as $social)
                                    @switch($social->key)
                                        @case ('instagram')
                                        <li class="list-inline-item">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال اینستگرام"
                                               data-placement="bottom"
                                               href="https://www.instagram.com/{{$social->value}}">
                                                <i class="fab fa-instagram fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                        @case ('telegram')
                                        <li class="list-inline-item ">
                                            <a class="footer-item" data-toggle="tooltip" title="کانال تلگرام"
                                               data-placement="bottom"
                                               href="https://www.t.me/{{$social->value}}">
                                                <i class="fab fa-telegram fa-2x text-white"></i>
                                            </a>
                                        </li>
                                        @break
                                    @endswitch
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="bg-bt-gold py-2">
    <div class="container">
        <div class="small text-center text-black font-size-15 font-family-aviny font-weight-bold">
            طراحی و اجرا توسط آکادمی نیما حمیدپور
        </div>
    </div>
</footer>
