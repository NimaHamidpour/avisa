<!DOCTYPE html>
<html dir="rtl" lang="fa">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta name="theme-color" content="#043261">

    <link rel="stylesheet" href="{{url('/css/app2.css?v=2')}}">
    <link rel="stylesheet" href="{{url('/css/fontawesome/css/all.min.css ')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

</head>
<body>

@include('app.top.header')

@yield('content')

@include('app.down.footer')

<script src="{{url('/JS/app2.js?v=2')}}"></script>


@yield('script')

<!--  mobile menu -->
<script>

    const openMenu = () => {
        document.getElementById('mobileMenu').classList.toggle('open');
    };

    const openSearchMobile = () => {
        document.getElementById('mobileSearch').classList.toggle('open');
    };

    // show and hide serachox
    $(document).mouseup(function (e) {
        var container = $("#search_div");

        // If the target of the click isn't the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('#search_box_id').hide('slow');
        }
    });

    function show_search_form() {
        $('#search_box_id').fadeToggle('slow');
    }

    $(document).ready(function () {
        // Prevent closing from click inside dropdown
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });
    });

</script>


<style type="text/css">
    @media all and (min-width: 992px) {
        .navbar {
            padding-top: 0;
            padding-bottom: 0;
        }

        .navbar .has-megamenu {
            position: static !important;
        }

        .navbar .megamenu {
            left: 0;
            right: 0;
            width: 100%;
            padding: 20px;
        }

        .navbar .nav-link {
            padding-top: 1rem;
            padding-bottom: 1rem;
        }
    }
</style>
</body>
</html>
