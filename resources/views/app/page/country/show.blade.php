@extends('app.index')




@section('content')


    <!-- HERO -->
    <section class="page-section">
        <div class="container-fluid">

            <div class="row mx-auto direction-rtl">

                <div class="col-12 mx-auto text-center">
                    <h1 class="align-center font-weight-bold font-family-yekan font-size-19 text-black mt-4">
                        {{$response['country']->title}}
                    </h1>
                    <hr class="divider"/>
                </div>
            </div>

            <div class="row mx-auto direction-ltr">

                <div class="col-3">
                              <span class="flag-icon flag-icon-{{$response['country']->flag}}"
                                    style="width: 300px;height: 350px"></span>
                </div>
                <div class="col-9 direction-rtl font-family-yekan font-size-15 text-black my-4 line-35">
                    {!! $response['country']->body !!}
                </div>
            </div>


        </div>
    </section>





@endsection


@section('script')
    <script>

    </script>
@endsection
