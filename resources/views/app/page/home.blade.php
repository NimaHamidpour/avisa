@extends('app.index')




@section('content')


    <!-- HERO -->
    <section id="hero" class="main-bg-gradient">
        <div class="container-fluid py-lg-5">
            <div class="row mx-auto direction-rtl py-5">


                <div class="col-md-6  line-35  margin-top-40 direction-rtl mx-auto text-center px-3" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration="1000">

                    <h1 class="align-center font-weight-bold font-family-aviny font-size-29 text-gold-grade mt-4">
                        {{$response['app_slogan']}}
                    </h1>
                    <div class="text-right justify-content-start direction-rtl font-family-yekan font-size-15 text-white my-4">
                       {!! $response['app_description'] !!}
                    </div>

                </div>


                <div class="col-md-6 align-center mt-3 d-none d-lg-block">
                    <img class="image-fluid img-fluid cssSelector" style="max-height: 300px!important;"
                         src="{{url('app/img/hero.png')}}" alt="{{$response['app_name']}}" />
                </div>



            </div>
        </div>
    </section>


    <!-- HERO -->
{{--    <div id="da-slider" class="da-slider">--}}
{{--        <div class="container-fluid h-100 m-0 p-0 main-slider">--}}
{{--            <div class="row m-0 p-0">--}}
{{--                <div class="col-12 m-0 p-0">--}}

{{--                    <div class="owl-carousel owl-theme slider-position" id="main_slider">--}}
{{--                        @foreach($response['slider'] as $slider)--}}
{{--                            <div class="item">--}}
{{--                                <img src="{{url($slider->src[400])}}" alt="{{$slider->title}}"--}}
{{--                                     class="img-fluid mainSliderImage">--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}




    <section class="ftco-services ftco-no-pb" id="Why-us">
        <div class="container-wrap">
            <div class="row no-gutters">

                <div class="col-md-3 d-flex services align-self-stretch py-5 px-4  main-bg-gradient" data-aos="fade-up"
                     data-aos-easing="linear"
                     data-aos-duration="1000">
                    <div class="media block-6 d-block text-center mx-auto">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{url('app/img/why-us/seeds.svg')}}" class="card-img-top height-50width-50 mx-auto " alt="...">
                        </div>
                        <div class="media-body text-white p-2 mt-3">

                            <h2 class="heading font-size-22 font-weight-bold font-family-yekan text-gold-grade">
                                کیفیت بالا
                            </h2>
                            <p class="mt-4 font-family-yekan text-gold-grade">
                                کیفیت بالا
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 d-flex services align-self-stretch py-5 px-4 bg-bt-gold" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration="1000">
                    <div class="media block-6 d-block text-center mx-auto">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{url('app/img/why-us/best.svg')}}" class="card-img-top height-50width-50 mx-auto " alt="...">
                        </div>
                        <div class="media-body text-white p-2 mt-3">
                            <h2 class="heading font-size-22 font-weight-bold font-family-yekan text-black">
                               بهترین در ایران
                            </h2>
                            <p class="mt-4  font-family-yekan text-black">
                                بهترین در ایران
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 d-flex services align-self-stretch py-5 px-4 main-bg-gradient" data-aos="fade-up"
                     data-aos-easing="linear"
                     data-aos-duration="1000">
                    <div class="media block-6 d-block text-center mx-auto">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{url('app/img/why-us/money.svg')}}" class="card-img-top height-50width-50 mx-auto " alt="...">
                        </div>
                        <div class="media-body text-white p-2 mt-3">
                            <h2 class="heading font-size-22 font-weight-bold font-family-yekan text-gold-grade">
                                قیمت مناسب
                            </h2>
                            <p class="mt-4 font-family-yekan text-gold-grade">
                                قیمت مناسب
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 d-flex services align-self-stretch py-5 px-4  bg-bt-gold" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration="1000">
                    <div class="media block-6 d-block text-center mx-auto">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <img src="{{url('app/img/why-us/support.svg')}}" class="card-img-top height-50width-50 mx-auto " alt="...">
                        </div>
                        <div class="media-body p-2 mt-3 text-white">
                            <h2 class="heading font-size-22 font-weight-bold font-family-yekan text-black">
                                پشتیبانی آنلاین
                            </h2>
                            <p class="mt-4 font-family-yekan text-black">
                                پشتیبانی آنلاین
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


    @if(count($response['aboutUs']) > 0)
        <section class="page-section services direction-ltr pb-4" id="about">
            <div class="container">

                <div class="row mb-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="text-center mt-5 font-size-19 font-yekan font-weight-bold">
                           {{$response['app_name']}}
                        </h3>
                        <hr class="divider mb-1">
                    </div>
                </div>

                <div class="about pos-z-index my-5">
                    <div class="row">

                        <!-- IMG -->
                        @for($i=0;$i<count($response['aboutUs']);$i++)
                            <div class="images col-md-6 {{$i===0 ? 'd-block' : 'd-none'}}" id="about_img_{{$i}}">
                                <div class="imageHolder">
                                    <img class="image" style="height: 410px"
                                         src="{{url($response['aboutUs'][$i]['image']['400'])}}"
                                         alt="{{$response['aboutUs'][$i]['title']}}"/>
                                </div>
                            </div>
                          @endfor


                    <!-- PRE && NEXT -->
                        <div
                            class="control col-xl-1 col-lg-1 col-md-1 col-sm-12 col-xs-12 d-flex direction-rtl flex-column justify-content-center">
                            <button class="navBtn up shadow-none"
                                    onclick="aboutUs_slideShow('plus',{{count($response['aboutUs'])-1}})">
                                <svg>
                                    <use xlink:href="{{url('app/img/sprite.svg#chevron')}}"></use>
                                </svg>
                            </button>
                            <button class="navBtn down shadow-none"
                                    onclick="aboutUs_slideShow('plus',{{count($response['aboutUs'])-1}})">
                                <svg>
                                    <use xlink:href="{{url('app/img/sprite.svg#chevron')}}"></use>
                                </svg>
                            </button>
                        </div>


                        <!-- TEXT -->
                        <div class="contents col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            @for($i=0;$i<count($response['aboutUs']);$i++)
                                <div class="{{$i===0 ? 'd-block' : 'd-none'}} direction-rtl"
                                     id="about_content_{{$i}}">
                                    <h2 class="font-size-19 font-weight-bold text-center mb-4">{{$response['aboutUs'][$i]['title']}}</h2>
                                    <div>
                                        <p class="font-size-14 line-35 text-justify px-2">
                                            {!! str_replace(PHP_EOL,"<br/>",$response['aboutUs'][$i]['body']) !!}
                                        </p>
                                    </div>
                                </div>
                            @endfor
                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endif




    <!-- value Section -->
    <section class="page-section direction-ltr bg-white p-0 mt-5" id="value">
        <div class="container-fluid">
            <div class="row border-radius-22">
                <div class="col-md-4 text-center p-4 main-bg-gradient">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 text-center my-4">
                            <div class="mt-2">
                                <img src="{{url('app/img/svg/experience.svg')}}"
                                     class="card-img-top height-50width-50 mx-auto img-fluid" alt="...">
                                <p class="font-size-19 text-white text-center margin-17 my-2">
                                    <span class="counter font-family-aviny font-size-23">15</span></p>
                                <p class="my-1 text-gold-grade font-weight-bold font-size-14">
                                    سال تجربه کاری
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center my-4">
                            <div class="mt-2">
                                <img src="{{url('app/img/svg/success.svg')}}"
                                     class="card-img-top height-50width-50 mx-auto img-fluid" alt="...">
                                <p class="font-size-19 text-white text-center margin-17 my-2">
                                    <span class="counter font-family-aviny font-size-23">420</span>
                                </p>
                                <p class="my-1 text-gold-grade font-weight-bold font-size-14">
                                    پرونده موفق
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center my-4">
                            <div class="mt-2">
                                <img src="{{url('app/img/svg/consulting.svg')}}"
                                     class="card-img-top height-50width-50 mx-auto img-fluid" alt="...">
                                <p class="font-size-19 text-white text-center margin-17 my-2">
                                    <span class="counter font-family-aviny font-size-23">8</span></p>
                                <p class="my-1 text-gold-grade font-weight-bold font-size-14">
                                    کادر مشاوره مجرب
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 text-center my-4">
                            <div class="mt-2">
                                <img src="{{url('app/img/svg/paper.svg')}}"
                                     class="card-img-top height-50width-50 mx-auto img-fluid" alt="...">
                                <p class="font-size-19 text-white text-center margin-17 my-2">
                                    <span class="counter font-family-aviny font-size-23">250</span></p>
                                <p class="my-1 text-gold-grade font-weight-bold font-size-14">
                                    پرونده در دست بررسی
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="col-xl-8 col-lg-8 col-md-8 d-none d-xl-block d-lg-block d-md-block img-fluid border-t-r-18 bg-value"></div>
            </div>
        </div>
    </section>



    <!-- BLOG -->
    @if(isset($response['blogGroup']) && count($response['blogGroup']) > 0 )
        <section class="blog mb-5">
            <div class="container pos-z-index">
                <div class="row mb-4">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="text-center mt-5 font-size-19 font-yekan font-weight-bold">بلاگ</h3>
                        <hr class="divider mb-1">
                    </div>
                </div>
                <div class="tabs">
                    @php $active='active' @endphp
                    @foreach($response['blogGroup'] as $blogGroup)
                        <a class="{{$active}} blog_group cursor-pointer font-size-15" onclick="loadBlog(this,{{$blogGroup->id}})">{{$blogGroup->title}}</a>
                        @php $active='' @endphp
                    @endforeach
                </div>

                <div id="blogs-view">
                    <div class="carousel owl-carousel" id="blog-carousel">
                        @foreach($response['blogItem'] as $blogItem)
                            <div class="item">
                                <a href="{{route('app.blog.show',$blogItem->slug)}}" class="text-decoration-none">
                                    <figure>
                                        <img src="{{url($blogItem->image['300'])}}" alt="{{$blogItem->title}}">
                                    </figure>
                                    <h2 class="title text-center" style="min-height: 45px">{{$blogItem->title}}</h2>
                                    <p class="desc text-justify">
                                        {!! mb_substr(strip_tags($blogItem->description),0,200,'UTF8') !!}...
                                    </p>
                                    <div class="date">{{jdate($blogItem->created_at)->format('Y %B')}}</div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </section>
    @endif




@endsection


@section('script')
    <script>
        $("#main_slider").owlCarousel({
            loop: true,
            autoplay: true,
            items: 1,
            rtl: true,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1000: {
                    items: 1,
                    height:400

                }
            }
        });

        jQuery(document).ready(function ($) {
            $("h3.counter").counterUp({
                delay: 75,
                time: 6000,
                offset: 100,
            });
        });
        AOS.init();
    </script>
@endsection
