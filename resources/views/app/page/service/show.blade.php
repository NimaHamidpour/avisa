@extends('app.index')




@section('content')


    <!-- HERO -->
    <section class="page-section">
        <div class="container">
            <div class="row mx-auto direction-rtl">

                <div class="col-12 mx-auto text-center">
                    <h1 class="align-center font-weight-bold font-family-yekan font-size-19 text-black mt-4">
                        {{$response['service']->title}}
                    </h1>
                    <hr class="divider"/>
                </div>

{{--                <div class="col-12 mx-auto text-center px-3">--}}

{{--                    <img src="{{url($response['service']->image['900'])}}" alt="{{$response['service']->title}}" class="img-fluid rounded"/>--}}

{{--                </div>--}}



                <div class="col-12 mx-auto text-center">
                    <div class="direction-rtl font-family-yekan font-size-15 text-black my-4 line-35">
                        {!! $response['service']->body !!}
                    </div>
                </div>

            </div>
        </div>
    </section>





@endsection


@section('script')
    <script>

    </script>
@endsection
