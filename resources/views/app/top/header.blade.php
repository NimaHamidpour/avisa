<!-- public Menu -->
{{--<div class="container-fluid d-none d-lg-block direction-rtl px-5 py-2 main-bg-gradient">--}}
{{--    <div class="row pt-1 my-0 border-top-gold">--}}

{{--        <div--}}
{{--            class="col-6 my-auto direction-rtl text-right d-none d-xl-inline-block d-lg-inline-block">--}}
{{--            <h1 class="font-family-aviny font-size-25  mx-auto text-white">--}}
{{--                {{$response['app_name']}}--}}
{{--            </h1>--}}
{{--            <h2 class="mt-1 font-family-yekan font-size-14  font-weight-bold mx-auto text-gold-grade d-none d-xl-inline-block d-lg-inline-block">--}}
{{--                {{$response['app_slogan']}}--}}
{{--            </h2>--}}
{{--        </div>--}}

{{--        <div--}}
{{--            class="col-6 my-auto text-left direction-ltr d-none d-xl-inline-block d-lg-inline-block">--}}
{{--            <a href="{{route('app.home')}}" class="text-decoration-none">--}}
{{--                <img src="{{url($response['app_logo'])}}" class="img-fluid" style="max-width: 80px!important;">--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


<!-- MENU DESKTOP -->
<div class="container-fluid d-none d-lg-block mx-0 px-0 bg-bt-gold">
    <nav class="navbar navbar-expand-lg navbar-light  px-3
                sticky-top py-1  direction-rtl" style="box-shadow: 3px 3px 10px #c1b888;"
         id="main-navigation">

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav direction-rtl">

                <li class="nav-item active align-right mx-2">
                    <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold"
                       href="{{route('app.home')}}">
                        صفحه اصلی
                    </a>
                </li>

                <li class="nav-item  align-right  mx-2">
                    <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold"
                       href="#" data-toggle="dropdown">
                        معرفی کـشورها
                        <i class="fa fa-angle-down font-size-11 mt-2"></i>
                    </a>
                    <div class="dropdown-menu megamenu" role="menu" style="max-width:700px;">
                        <div class="row direction-rtl">
                            @foreach($response['countries']->chunk(4) as $countries)
                                <div class="col-md-4 text-right p-0">
                                    <div class="col-megamenu">
                                        <ul class="list-unstyled  p-0">
                                            @foreach($countries as $country)
                                                <li class="mb-4">
                                                    <a class="text-decoration-none"
                                                       href="{{route('app.country.show',$country->slug)}}">
                                                                <span
                                                                    class="flag-icon flag-icon-{{$country->flag}}"
                                                                    style="height: 20px;width: 40px"></span>
                                                        <span
                                                            class="text-dark font-size-14 font-weight-bold mt-5">{{$country->title}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </li>

                @foreach($response['serviceGroup'] as $serviceGroup)
                    <li class="nav-item  align-right  mx-2">
                        @if(count($serviceGroup->Service) > 0)
                            <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold"
                               href="#" data-toggle="dropdown">
                                {{$serviceGroup->title}}
                                <i class="fa fa-angle-down font-size-11"></i>
                            </a>
                            <div class="dropdown-menu megamenu" role="menu" style="max-width:700px;">
                                <div class="row direction-rtl">
                                    @foreach($serviceGroup->Service->chunk(4) as $services)
                                        <div class="col-md-4 text-right p-0">
                                            <div class="col-megamenu">
                                                <ul class="list-unstyled  p-0">
                                                    @foreach($services as $service)
                                                        <li class="mb-4">
                                                            <a class="text-decoration-none"
                                                               href="{{route('app.service.show',$service->slug)}}">
                                                                <span
                                                                    class="flag-icon flag-icon-{{$service->Country->flag}}"
                                                                    style="height: 20px;width: 40px"></span>
                                                                <span
                                                                    class="text-dark font-size-14 font-weight-bold mt-5">{{$service->title}}</span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold" href="#">
                                {{$serviceGroup->title}}
                            </a>
                        @endif

                    </li>
                @endforeach

                @foreach($response['menu'] as $menu)
                    <li class="nav-item  align-right  mx-2">

                        @if(count($menu->Child()->get()) > 0)
                            <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold"
                               href="#" data-toggle="dropdown">
                                {{$menu->title}}
                                <i class="fa fa-angle-down font-size-11 mt-2"></i>
                            </a>
                            <div class="dropdown-menu megamenu" role="menu" style="max-width:700px;">
                                <div class="row direction-rtl">
                                    @foreach($menu->Child()->chunk(4) as $menuItems)
                                        <div class="col-md-4 text-right p-0">
                                            <div class="col-megamenu">
                                                <ul class="list-unstyled  p-0">
                                                    @foreach($menuItems as $menuItem)
                                                        <li class="mb-4">
                                                            <a class="text-decoration-none"
                                                               href="{{route('app.page.show',$menuItems->url)}}">
                                                                <span class="text-dark font-size-14 font-weight-bold">
                                                                     {{$menuItems->title}}
                                                                </span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <a class="nav-link text-dark font-family-yekan font-size-14 font-weight-bold"
                               href="{{route('app.page.show',$menu->url)}}">
                                {{$menu->title}}
                            </a>
                        @endif

                    </li>
                @endforeach

            </ul>
        </div>
    </nav>
</div>


<!-- MENU MOBILE -->
<div class="container-fluid d-block d-lg-none mx-0 px-0">
    <div class="col-12 main-bg-gradient mx-0 px-0 direction-rtl">
        <div class="d-flex text-center position-relative py-2">

            <button class="menuBtn btn py-0 my-0" onclick="openMenu()">
                <img src="{{url('app/img/bar.png')}}" height="15px">
            </button>

            <div class="mx-auto">
                <a href="{{route('app.blog.home')}}"
                   class="font-family-yekan text-decoration-none text-gold-grade
                        font-size-15 font-weight-bold">
                    {{ $response['app_name']}}
                </a>
            </div>

            <div
                class="my-auto text-left direction-ltr px-2">
                <a href="{{route('app.home')}}" class="text-decoration-none">
                    <img src="{{url($response['app_logo'])}}" class="img-fluid" style="max-width: 30px!important;">
                </a>
            </div>

        </div>

        <nav class="mainNav" id="mobileMenu">

            <div class="mb-5 d-flex d-md-none w-100 justify-content-between align-items-center">
                <a href="{{route('app.blog.home')}}"
                   class="font-family-yekan text-decoration-none text-white font-size-17 font-weight-bold">
                    <img src=" {{url($response['app_logo'])}}" height="65px">
                </a>
                <button class="close font-38" onclick="openMenu()">
                    <i class="fas fa-times text-white"></i>
                </button>
            </div>

            <div class="text-right px-0">

                <ul class="list-unstyled px-1">

                    <li class="text-right align-center">
                        <a href="{{route('app.home')}}"
                           class="text-right font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none">
                            صفحه اصلی
                        </a>
                    </li>

                    <li class="text-right dropdown">
                        <a href="#" data-toggle="dropdown"
                           class="mx-auto text-center font-family-yekan font-weight-normal dropdown-toggle
                                       font-size-14 color-blog-secondary text-decoration-none"
                           role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            معرفی کشورها
                        </a>
                        <div class="dropdown-menu text-right" role="menu"
                             aria-labelledby="navbarDropdownCountry">

                            @foreach($response['countries'] as $country)
                                <a class="dropdown-item text-decoration-none mb-2"
                                   href="#">
                                                       <span class="flag-icon flag-icon-{{$country->flag}}"
                                                             style="height: 20px;width: 40px"></span>
                                    <span class="text-dark font-size-14 font-weight-bold mt-5">
                                                            {{$country->title}}
                                                        </span>
                                </a>

                            @endforeach
                        </div>
                    </li>
                    @foreach($response['serviceGroup'] as $serviceGroup)
                        @if(count($serviceGroup->Service) > 0)
                            <li class="text-right dropdown">
                                <a href="#" data-toggle="dropdown"
                                   class="mx-auto text-center font-family-yekan font-weight-normal dropdown-toggle
                                       font-size-14 color-blog-secondary text-decoration-none"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$serviceGroup->title}}
                                </a>
                                <div class="dropdown-menu text-right" role="menu"
                                     aria-labelledby="navbarDropdown{{$serviceGroup->id}}">

                                    @foreach($serviceGroup->Service as $service)
                                        <a class="dropdown-item text-decoration-none mb-2"
                                           href="#">
                                                       <span class="flag-icon flag-icon-{{$service->Country->flag}}"
                                                             style="height: 20px;width: 40px"></span>
                                            <span class="text-dark font-size-14 font-weight-bold mt-5">
                                                            {{$service->title}}
                                                        </span>
                                        </a>

                                    @endforeach
                                </div>
                            </li>
                        @else
                            <li class="text-right">
                                <a href="{{$serviceGroup->url}}"
                                   class="text-right font-family-yekan font-weight-normal font-size-14 color-blog-secondary text-decoration-none">
                                    {{$serviceGroup->title}}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </nav>


    </div>
</div>





