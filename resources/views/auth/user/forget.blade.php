@extends('app.index')


@section('content')


    <section class="page-section">
        <div class="container py-5">
            @include('message.message')

        <div class="row pt-5">
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12 d-none d-xl-inline-block d-lg-inline-block">
                <img src="{{url('app/img/forget.png')}}" class="rounded mt-3 img-fluid">
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                 <div class="d-flex justify-content-center h-100">
                     <div class="user_card" style="height: 450px!important;">

                    <div class="d-flex justify-content-center">

                        <form method="POST" action="{{ route('user.forget') }}">
                        @csrf
                            <h2 class="font-yekan text-center font-size-19 text-danger mt-5 font-weight-bold">فراموشی کلمه عبور</h2>
                            <hr class="divider"/>

                        <!-- USER NAME -->
                           <div class="form-row">
                                   <label class="user-input-label" for="email">شماره موبایل</label>
                               <div class="input-group mb-4 direction-ltr">
                                   <div class="input-group-append">
                                       <span class="input-group-text"><i class="fas fa-user"></i></span>
                                   </div>
                                   <input type="text" name="mobile" id="mobile" class="form-control input_user font-family-iransans font-size-14 text-right" placeholder="شماره موبایل">
                               </div>
                           </div>


                            <div class="form-row my-4">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                    <label class="user-input-label d-block text-right" for="Capcha">وارد کردن تصویر امنیتی</label>
                                    <input type="text" class="user-input py-2 rounded shadow-none direction-ltr text-center" name="Capcha" id="Capcha">
                                    {!! captcha_img('math') !!}
                                </div>
                            </div>

                            <!-- LOGIN -->
                            <div class="d-flex justify-content-center mt-4 login_container">
                                <button type="submit" name="loginadmin" class="btn login_btn font-yekan mt-3">بازیابی کلمه عبور</button>
                            </div>

                        </form>

                    </div>

                </div>
                 </div>

            </div>
        </div>
        </div>
    </section>
@endsection
