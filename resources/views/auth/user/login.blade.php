@extends('app.index')


@section('content')


    <section class="page-section">
        <div class="container py-5">
            <div class="row">
                <div class="col-12">
                    @include('message.message')

                </div>
            </div>
            <div class="row">

                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12 mt-5 pt-5 d-none d-xl-inline-block d-lg-inline-block">
                    <img src="{{url('app/img/login.png')}}" class="rounded my-auto  img-fluid">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                    <div class="d-flex justify-content-center h-100 my-0 my-xl-3 my-lg-3">

                        <div class="user_card">
                            <h2 class="font-family-yekan text-center font-size-22 text-danger mt-0 mt-xl-5 mt-lg-5 font-weight-bold">ورود به حــساب کــاربری</h2>
                            <hr class="divider"/>

                            <div class="d-flex justify-content-center">
                                <form method="POST" action="{{ route('login') }}" class="px-2">
                                @csrf
                                <!-- USER NAME -->

                                    <div class="form-row">
                                        <label class="font-family-yekan font-size-12 text-right font-weight-bold">نام کاربری</label>

                                        <div class="input-group mb-3 direction-ltr">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <input type="text" name="username" autocomplete="off" class="form-control input_user font-family-yekan font-size-14 text-right" placeholder="ایمیل یا شماره تماس">
                                        </div>
                                    </div>

                                    <!-- PASSWORD -->
                                    <div class="form-row mt-1">
                                        <label class="font-family-yekan font-size-12 text-right font-weight-bold">کلمه عبور</label>
                                        <div class="input-group mb-2 direction-ltr">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                                            </div>
                                            <input type="password" name="password" class="form-control input_pass font-size-14 font-family-yekan text-right" placeholder="کلمه عبور">
                                        </div>
                                    </div>

                                    <!-- REGISTER -->
                                    <div class="form-group border-bottom">
                                        <div class="custom-control custom-checkbox direction-rtl text-right my-2">
                                            <a href="{{route('register')}}" class="text-decoration-none">
                                                <span class="font-family-yekan font-size-14 user-danger">تا الان ثبت نام نکردید،</span>
                                                <span class="font-family-yekan font-size-14 user-success">ثبت نام کنید</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="form-row my-4">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                            <label class="admin-input-label d-block text-right" for="Capcha">وارد کردن تصویر امنیتی</label>
                                            <input type="text" class="user-input py-2 rounded shadow-none direction-ltr text-center" name="Capcha" id="Capcha">
                                            {!! captcha_img('math') !!}
                                        </div>
                                    </div>


                                    <!-- LOGIN -->
                                    <div class="d-flex justify-content-center mt-3 login_container">
                                        <button type="submit" name="loginadmin" class="btn login_btn font-family-yekan">ورود</button>
                                    </div>

                                </form>
                            </div>

                            <div class="mt-4">
                                <div class="d-flex justify-content-center links">
                                    <a href="{{route('user.forget.form')}}" class="text-decoration-none user-danger font-family-yekan font-size-13 font-weight-bold">رمز عبور خود را فراموش کرده ام</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
