@extends('app.index')

@section('title','فرم ارزشیابی رایگان')
@section('content')

    <!-- HEADER -->
    <section class="page-section my-5">
        <div class="container">

            @include('message.message')
            <section class="direction-rtl bg-white mb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto">
                            <form action="{{route('app.register.store')}}" method="POST"
                                  class="admin-form text-right direction-rtl border my-3  py-1 px-4">
                                @csrf

                                <h2 class="font-aviny text-center font-size-23 text-gold font-weight-bold">
                                    فرم ارزیابی رایگان
                                </h2>
                                <hr class="divider"/>

                                <div class="form-row">

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto my-3">
                                        <label for="name-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;نام</label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center user-input rounded-0 @error('name') is-invalid @enderror"
                                               id="name-id" name="name" placeholder="نام و فامیل"
                                               value="{{old('name')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="mobile-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;شماره
                                            موبایل
                                            </label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('mobile') is-invalid @enderror"
                                               id="mobile-id" name="mobile" placeholder="۰۹ ..."
                                               value="{{old('mobile')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="sex-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;جنسیت</label>
                                        <select class="form-control user-input text-option-center" name="sex"
                                                id="sex-id">
                                            <option value="male">آقا</option>
                                            <option value="female">خانم</option>
                                        </select>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="degree-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;آخرین
                                            مدرک تحصیلی</label>
                                        <select class="form-control user-input text-option-center" name="degree"
                                                id="degree-id">
                                            <option value="student">دانش آموز</option>
                                            <option value="diploma">دیپلم</option>
                                            <option value="illiterate">فوق دیپلم</option>
                                            <option value="bachelor">لیسانس</option>
                                            <option value="master">فوق لیسانس</option>
                                            <option value="phd">دکتری</option>
                                        </select>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="field-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;رشته
                                            تحصیلی</label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center admin-input rounded-0 @error('field') is-invalid @enderror"
                                               id="field-id" name="field" placeholder="مهندسی برق - تجربی و..."
                                               value="{{old('field')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="married-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;وضعیت
                                            تاهل</label>
                                        <select class="form-control user-input text-option-center" name="married"
                                                id="married-id">
                                            <option value="yes">متاهل</option>
                                            <option value="no">مجرد</option>
                                        </select>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="child-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;
                                            تعداد فرزندان
                                        </label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('child') is-invalid @enderror"
                                               id="child-id" name="child" placeholder="0,1,2,..."
                                               value="{{old('child')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="mobile-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;تاریخ
                                            تولد</label>

                                        <div class="form-row direction-ltr">
                                            <div class="row text-center">
                                                <div class="col-4 p-0 pl-xl-4">
                                                    <input type="number"
                                                           class="form-control inputs text-center direction-ltr font-size-12 font-family-iransans @error('year') is-invalid @enderror"
                                                           id="year" value="{{old('year')}}" name="year"
                                                           placeholder="سال 4 رقمی"
                                                           maxlength="4" tabindex="1"/>
                                                </div>
                                                <div class="col-4 p-0">
                                                    <input type="number"
                                                           class="form-control inputs  text-center direction-ltr font-size-12 font-family-iransans @error('month') is-invalid @enderror"
                                                           id="month" value="{{old('month')}}" name="month"
                                                           placeholder="ماه"
                                                           maxlength="2" tabindex="2"/>
                                                </div>
                                                <div class="col-4 p-0 pr-xl-3">
                                                    <input type="number"
                                                           class="form-control inputs  text-center direction-ltr font-size-12 font-family-iransans @error('day') is-invalid @enderror"
                                                           id="day" value="{{old('day')}}" name="day" placeholder="روز"
                                                           maxlength="2" tabindex="3"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="language-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;وضعیت
                                            زبان انگلیسی</label>
                                        <select class="form-control user-input text-option-center" name="language"
                                                id="language-id">
                                            <option value="beginner">مقدماتی</option>
                                            <option value="intermediate">متوسط</option>
                                            <option value="Proficient">پیشرفته</option>
                                        </select>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="military-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;
                                            وضعیت سربازی
                                        </label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('military') is-invalid @enderror"
                                               id="military-id" name="military"
                                               placeholder="پایان خدمت ، معافیت ، عدم مشمولی"
                                               value="{{old('military')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="job-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;عنوان
                                            شغلی</label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('job') is-invalid @enderror"
                                               id="job-id" name="job" placeholder="برنامه نویس" value="{{old('job')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="insurance-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;وضعیت
                                            بیمه</label>
                                        <select class="form-control user-input text-option-center" name="insurance"
                                                id="insurance-id">
                                            <option value="yes">دارم</option>
                                            <option value="no">ندارم</option>
                                        </select>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="salary-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;
                                            میزان درآمد ماهیانه (میلیون)
                                            </label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('salary') is-invalid @enderror"
                                               id="salary-id" name="salary" placeholder="میزان درآمد ماهیانه"
                                               value="{{old('salary')}}">
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="experience-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;سابقه
                                            کار
                                            (سال)</label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('experience') is-invalid @enderror"
                                               id="experience-id" name="experience" placeholder="0,1,..."
                                               value="{{old('experience')}}">

                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto  my-3">
                                        <label for="budget-id" class="user-input-label"><i
                                                class="fas fa-pencil-alt prefix text-right text-admin "></i>&nbsp;میزان
                                            سرمایه شما
                                            (میلیون)</label>
                                        <input type="text"
                                               class="form-control font-size-14 text-center font-family-iransans direction-ltr rounded-0 @error('budget') is-invalid @enderror"
                                               id="budget-id" name="budget" placeholder="100"
                                               value="{{old('budget')}}">

                                    </div>


                                </div>

                                <div class="form-row mt-3">
                                    <input type="submit"
                                           class="col-md-4 btn admin-submit form-control mt-3 font-weight-normal"
                                           value="تایید و ثبت اطلاعات">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </section>

@endsection



