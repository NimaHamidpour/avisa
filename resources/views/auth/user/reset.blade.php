@extends('app.index')


@section('content')

    <section class="page-section">
        <div class="container py-5">
            @include('message.message')

        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <h2 class="font-yekan text-center text-danger mt-5 font-weight-bold">تغییر کلمه عبور</h2>
                <hr class="divider"/>
                <img src="{{url('app/img/reset.png')}}" class="rounded mt-3 img-fluid">
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mx-auto">

                 <div class="d-flex justify-content-center h-100">
                     <div class="user_card" style="height: 420px!important;">

                    <div class="d-flex justify-content-center">

                        <form method="POST" action="{{ route('user.reset') }}">
                            @csrf
                            <input type="hidden" value="{{$response['mobile']}}" name="mobile" readonly>
                            <div class="input-group mb-3 direction-ltr">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="token_code" class="form-control border input_user font-yekan text-right"
                                       placeholder="کد دریافتی" value="{{old('code')}}">
                            </div>

                            <div class="input-group mb-3 direction-ltr">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="password" name="password"
                                       class="form-control border input_user font-yekan text-right" placeholder="رمز عبور">
                            </div>

                            <div class="input-group mb-3 direction-ltr">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="password" name="password_confirmation"
                                       class="form-control border input_user font-yekan text-right"
                                       placeholder="تکرار رمز عبور">
                            </div>


                            <div class="form-row my-4">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mx-auto text-center">
                                    <label class="admin-input-label d-block text-right" for="Capcha">وارد کردن تصویر
                                        امنیتی</label>
                                    <input type="text" class="admin-input py-2 rounded shadow-none direction-ltr text-center"
                                           name="Capcha" id="Capcha">
                                    {!! captcha_img('math') !!}
                                </div>
                            </div>

                            <!-- LOGIN -->
                            <div class="d-flex justify-content-center mt-3 login_container">
                                <button type="submit" name="loginadmin" class="btn login_btn font-yekan">تغییر رمز عبور</button>
                            </div>

                        </form>

                    </div>

                </div>
                 </div>

            </div>
        </div>
        </div>
    </section>
@endsection
