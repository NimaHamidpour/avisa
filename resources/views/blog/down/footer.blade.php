<section class="page-section direction-ltr p-0">
    <div class="container-fluid bg-blog-main border-top-secondary">

        <div class="row mx-auto px-0 py-1">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center direction-rtl py-4">

                <div class="row mx-auto text-right px-1 px-xl-5 px-lg-5">
                    <h2 class="color-blog-secondary font-size-19">{{ $response['app_name']}}</h2>
                    <div class="font-size-14 my-4 line-35 font-family-yekan text-white text-justify">
                        {!! $response['blog_footer'] !!}
                    </div>
                </div>
            </div>
            <!-- SOCIAL -->
            <div class="row mx-auto">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    @foreach($response['socials'] as $social)
                        @switch($social->key)
                            @case ('instagram')
                            <li class="list-inline-item">
                                <a class="footer-item" data-toggle="tooltip" title="کانال اینستگرام"
                                   data-placement="bottom"
                                   href="https://www.instagram.com/{{$social->value}}">
                                    <i class="fab fa-instagram text-danger fa-2x"></i>
                                </a>
                            </li>
                            @break
                            @case ('telegram')
                            <li class="list-inline-item ">
                                <a class="footer-item" data-toggle="tooltip" title="کانال تلگرام"
                                   data-placement="bottom"
                                   href="https://www.t.me/{{$social->value}}">
                                    <i class="fab fa-telegram text-white fa-2x"></i>
                                </a>
                            </li>
                            @break
                            @case ('whatsapp')
                            <li class="list-inline-item ">
                                <a class="footer-item" data-toggle="tooltip" title="کانال وتس اپ"
                                   data-placement="bottom"
                                   href="https://www.whatsapp.com/{{$social->value}}">
                                    <i class="fab fa-whatsapp text-success"></i>
                                </a>
                            </li>
                            @break
                        @endswitch

                    @endforeach
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer class="row bg-blog-secondary py-2">
            <div class="container">
                <div class="text-center text-white font-size-14 direction-rtl">*کلیه حقوق مادی و معنوی این وبسایت متعلق به {{$response['app_name']}} می باشد* </div>
            </div>
        </footer>

    </div>
</section>
<!-- /services Section -->
