<!-- LEFT SIDE -->
<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 text-center mx-auto">
    <h3 class="font-weight-bold font-family-yekan border-bottom-secondary font-size-15 text-black pb-2 mx-auto width-20"> آخرین مقالات</h3>
    @foreach(  $response['most_visited'] as $most_visited)
        <div class="card border-none mx-1 pt-2">
            <a class="text-black text-decoration-none cursor-pointer blog-hover" href="{{route('app.blog.show',$most_visited->slug)}}">
                <img class="card-img-top card-img-blog" src="{{url($most_visited->image['300'])}}" alt="{{$most_visited->title}}">
                <div class="card-body px-0">
                    <h2 class="card-title font-size-15 font-family-yekan text-right">{{$most_visited->title}}</h2>
                    <div class="text-right font-size-11 text-secondary font-family-iransans mb-2">
                        {{jdate($most_visited->created_at)->format('Y.m.d')}}
                        <span class="view-counter font-family-iransans">{{$most_visited->view_count}}</span>
                    </div>
                    <div class="card-text font-size-14 text-muted text-right">
                        {!! mb_substr(strip_tags($most_visited->description),0,125,'UTF8').'...' !!}
                    </div>

                </div>
            </a>
        </div>
    @endforeach
</div>
