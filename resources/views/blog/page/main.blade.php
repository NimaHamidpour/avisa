@extends('blog.index')


@section('content')

    <section class="page-section direction-rtl p-0">
        <div class="container-fluid bg-white pt-5">
            <div class="row">

                <!-- LAST UPLOAD -->
                <div class="col-lg-7 col-md-7 col-md-12 col-sm-12 col-xs-12 text-center mx-auto">
                    <div class="row direction-rtl mb-4">
                        @foreach($response['blogItem'] as $blogItem)
                            <div class="card border-none mb-4 mr-4">
                                <a class="d-content text-decoration-none cursor-pointer blog-hover"  href="{{route('app.blog.show',$blogItem->slug)}}">
                                    <div class="row no-gutters">

                                        <!-- IMG -->
                                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 pr-2">
                                            <img src="{{url($blogItem->image['300'])}}"  class="card-img card-img-blog" alt="{{$blogItem->title}}">
                                        </div>

                                        <!-- TEXT -->
                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <div class="card-body">
                                                <h2 class="card-title text-right font-size-15 font-family-yekan font-weight-bold text-black">{{$blogItem->title}}</h2>
                                                <div class="col-12 text-right my-2 mx-0 px-0">
                                                    <a class="blog-category-link font-family-iransans font-size-14" href="{{route('app.blog.home',$blogItem->Category->slug)}}">
                                                        {{$blogItem->Category->title}}
                                                    </a>
                                                    <span class="card-title text-right font-size-11 text-secondary font-family-iransans mr-1">
                                                             {{jdate($blogItem->created_at)->format('Y.m.d')}}
                                                     </span>
                                                    <span class="view-counter font-family-iransans">{{$blogItem->view_count}}</span>
                                                </div>
                                                <div class="card-text text-justify text-muted font-family-yekan mt-3 font-size-14">
                                                    {!! mb_substr(strip_tags($blogItem->description),0,200,'UTF8').'...' !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>

                @include('blog.page.leftSide')

            </div>
        </div>
    </section>

@endsection


