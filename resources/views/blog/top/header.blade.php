<div class="container-fluid bg-white border-top-red">

    <!-- TIME AND SOCIAL -->
    <div class="row bg-light py-2 nav-box-shadow">

        <!-- TIME-->
        <div class="col-6 text-right">
            <div>
                <span class="mb-3 font-family-iransans font-size-11 font-weight-bold">{{jdate()->format('%A, %d %B %y')}}</span>
            </div>
        </div>

        <!-- SOCIAL MEDIA -->
        <div class="col-6 text-left">
            @foreach($response['socials'] as $social)
                @switch($social->key)
                    @case ('instagram')
                    <li class="list-inline-item ">
                        <a class="footer-item" data-toggle="tooltip" title="کانال اينستگرام"
                           data-placement="bottom"
                           href="https://www.instagram.com/{{$social->value}}">
                            <i class="fab fa-instagram text-danger font-weight-bold"></i>
                        </a>
                    </li>
                    @break
                    @case ('telegram')
                    <li class="list-inline-item ">
                        <a class="footer-item" data-toggle="tooltip" title="کانال تلگرام"
                           data-placement="bottom"
                           href="https://www.t.me/{{$social->value}}">
                            <i class="fab fa-telegram text-dark"></i>
                        </a>
                    </li>
                    @break
                    @case ('whatsapp')
                    <li class="list-inline-item ">
                        <a class="footer-item" data-toggle="tooltip" title="کانال وتس اپ"
                           data-placement="bottom"
                           href="https://www.whatsapp.com/{{$social->value}}">
                            <i class="fab fa-whatsapp text-dark"></i>
                        </a>
                    </li>
                    @break
                @endswitch

            @endforeach
        </div>

    </div>


    <!-- MENU -->
    <div class="row">

        <!-- MENU DESKTOP -->
        <nav class="navbar navbar-expand-lg w-100 p-0 m-0 border-bottom-red direction-ltr d-none d-lg-block" id="blogNav">
            <div class="container-fluid">

                <div id="search_div">
                    <button class="btn ml-4 border-none shadow-none" id="search_btn" onclick="show_search_form()">
                        <i class="fa fa-search text-white"></i>
                    </button>

                    <div class="col-4 border-top-main bg-light p-3 search_form" style="display: none" id="search_box_id">
                        <form action="{{route('app.blog.search')}}" method="POST">
                            @csrf
                            <div class="input-group">
                                <input type="submit" class="btn bg-blog-secondary font-family-yekan text-white rounded-0 font-size-13
                                            px-3 text-center border-none" style="height: 35px!important" value="جستجو" onclick="search()">
                                <input type="text" style="height: 34px!important;"
                                       class="form-control text-right font-family-iransans font-size-11
                                       shadow-none direction-rtl text-black" autocomplete="off"
                                       onkeyup="suggest_search_blog()" id="search_text_id" name="search_text_name">
                            </div>

                            <div class="border-top mt-3 direction-rtl text-center" id="aj_search_content">

                            </div>

                        </form>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto my-2 my-lg-0 direction-rtl">

                        <li class="nav-item align-center mx-1">
                            <a  class="nav-link js-scroll-trigger font-family-yekan direction-ltr font-size-14 font-weight-normal menu-blog my-2"
                                    style="color: white!important;"
                                href="{{route('app.home')}}">
                                   {{$response['app_name']}}
                                <i class="fa fa-home color-blog-secondary"></i>
                            </a>
                        </li>

                        @foreach($response['categories'] as $category)
                            <li class="nav-item align-center">
                                <a  class="nav-link js-scroll-trigger font-family-yekan font-size-14 font-weight-normal my-2"
                                    style="color: white!important;"
                                    href="{{route('app.blog.home',$category->slug)}}">
                                    {{$category->title}}
                                </a>
                            </li>
                        @endforeach

                    </ul>
                </div>

            </div>
        </nav>
        <!-- MENU DESKTOP -->

        <!-- MENU MOBILE -->
        <div class="col-12 d-block d-lg-none main-bg-gradient direction-rtl">
            <div class="d-flex justify-content-between position-relative d-md-none d-block p-1">

                <button class="menuBtn btn " onclick="openMenu()" >
                    <i class="fas fa-bars font-size-22 color-blog-secondary bold"></i>
                </button>

                <div class="d-inline-block mx-auto mt-2">
                    <a href="{{route('app.blog.home')}}" class="font-family-yekan text-decoration-none  font-size-14  font-weight-bold color-blog-secondary">{{ $response['app_name']}}</a>
                </div>

                <button class="bg-transparent border-none shadow-none" id="search_btn" onclick="openSearchMobile()">
                    <i class="fa fa-search color-blog-secondary"></i>
                </button>
            </div>

            <nav class="mainNav" id="mobileMenu">
                <div class="mb-5 d-flex d-md-none w-100 justify-content-between align-items-center">
                    <a href="{{route('app.blog.home')}}" class="font-family-yekan text-decoration-none text-white font-size-17 font-weight-bold">
                       <img src=" {{url($response['app_logo'])}}" height="50px">
                    </a>
                    <button class="close font-38" onclick="openMenu()">
                        <i class="fas fa-times text-white"></i>
                    </button>
                </div>

                <ul class="list-unstyled bold">
                    <li>
                        <a  href="{{route('app.home')}}" class="direction-ltr font-family-yekan font-weight-bold font-size-14 text-gold-grade text-decoration-none">
                              {{$response['app_name']}}
                        </a>
                    </li>
                    @foreach($response['categories'] as $category)
                        <li>
                            <a  href="{{route('app.blog.home',$category->slug)}}" class="font-family-yekan font-weight-bold font-size-14 text-gold-grade text-decoration-none">
                                {{$category->title}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </nav>

            <div class="mainNav" id="mobileSearch">

                <div class="mb-5 d-flex d-md-none w-100 justify-content-between align-items-center">
                    <button class="close font-size-36" onclick="openSearchMobile()">
                        <i class="fas fa-times text-white"></i>
                    </button>
                </div>

                <div class="row">
                    <div class="col-11 search_form m-0 p-0" id="search_box_id">
                        <form action="{{route('app.blog.search')}}" method="post" class="mr-2">
                            @csrf
                            <h2 class="font-size-12 text-white text-center mb-4 font-family-yekan">جستجو</h2>
                            <input type="text" style="height: 34px!important;"
                                   class="form-control text-right font-family-iransans font-size-11
                                       shadow-none direction-rtl text-white border-bottom border-top-0 border-left-0 border-right-0 rounded-0 bg-black" autocomplete="off"
                                   onkeyup="suggest_search_blog_mobile()" id="search_text_mobile_id" name="search_text_name">
                            <div class="mt-3 pt-3 direction-rtl" id="aj_search_content_mobile">

                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- MENU MOBILE -->

    </div>

</div>
