<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// ****************************************** admin *********************************************

//  DASHBOARD
Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('پنل مدیریت', route('admin.dashboard'));
});

//  SETTING
Breadcrumbs::register('admin.setting', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('مدیریت تنظیمات سایت');
});

//  SOCIAL
Breadcrumbs::register('admin.social', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('مدیریت شبکه های اجتماعی');
});

//****************************************************************************
//  CATEGORY LIST
Breadcrumbs::register('admin.category.list', function ($breadcrumbs,$type) {

    $breadcrumbs->parent('admin.dashboard');
    if ($type === 'blog')
         $breadcrumbs->push('لیست دسته بندی های بلاگ ' , route('admin.category.list',$type));
    else
         $breadcrumbs->push('لیست دسته بندی های خدمات ',route('admin.category.list',$type));
});


//  CATEGORY EDIT
Breadcrumbs::register('admin.category.edit', function ($breadcrumbs,$category) {

    $breadcrumbs->parent('admin.category.list',$category->type);
    $breadcrumbs->push('ویرایش دسته ' . $category->title);

});

//****************************************************************************
//  Page LIST
Breadcrumbs::register('admin.page.list', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('لیست صفحات وب سایت',route('admin.page.list'));

});

//  Page ADD
Breadcrumbs::register('admin.page.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.page.list');
    $breadcrumbs->push('افزودن صفحه جدید');
});

//  Page EDIT
Breadcrumbs::register('admin.page.edit', function ($breadcrumbs,$page) {

    $breadcrumbs->parent('admin.page.list');
    $breadcrumbs->push('ویرایش صفحه : '. $page->title);
});

//****************************************************************************
//  BLOG LIST
Breadcrumbs::register('admin.blog.list', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('لیست مطالب وب سایت',route('admin.blog.list'));

});

//  BLOG ADD
Breadcrumbs::register('admin.blog.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.blog.list');
    $breadcrumbs->push('افزودن مطلب جدید');
});

//  BLOG EDIT
Breadcrumbs::register('admin.blog.edit', function ($breadcrumbs,$blog) {

    $breadcrumbs->parent('admin.blog.list');
    $breadcrumbs->push('ویرایش مطلب : '. $blog->title);
});


//****************************************************************************
//  Country LIST
Breadcrumbs::register('admin.country.list', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('لیست کشورها',route('admin.country.list'));

});

//  Country ADD
Breadcrumbs::register('admin.country.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.country.list');
    $breadcrumbs->push('افزودن کشور جدید');
});

//  Country EDIT
Breadcrumbs::register('admin.country.edit', function ($breadcrumbs,$country) {

    $breadcrumbs->parent('admin.country.list');
    $breadcrumbs->push('ویرایش کشور : '. $country->title);
});


//****************************************************************************
//  Service LIST
Breadcrumbs::register('admin.service.list', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('لیست خدمات',route('admin.service.list'));

});

//  Service ADD
Breadcrumbs::register('admin.service.create', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.service.list');
    $breadcrumbs->push('افزودن خدمت جدید');
});

//  Service EDIT
Breadcrumbs::register('admin.service.edit', function ($breadcrumbs,$service) {

    $breadcrumbs->parent('admin.service.list');
    $breadcrumbs->push('ویرایش خدمت : '. $service->title);
});

//****************************************************************************
//  Menu LIST
Breadcrumbs::register('admin.menu.list', function ($breadcrumbs) {

    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('لیست منوها',route('admin.menu.list'));

});

//  Menu EDIT
Breadcrumbs::register('admin.menu.edit', function ($breadcrumbs,$menu) {

    $breadcrumbs->parent('admin.menu.list');
    $breadcrumbs->push('ویرایش منو : '. $menu->title);
});
//****************************************************************************

//  LIST IMAGE
Breadcrumbs::register('admin.image.list', function ($breadcrumbs)
{
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('تصاویر سایت',route('admin.image.list'));
});
//  EDIT IMAGE
Breadcrumbs::register('admin.image.edit', function ($breadcrumbs,$image)
{
    $breadcrumbs->parent('admin.image.list');
    $breadcrumbs->push('ویرایش تصویر : ' . $image->title);
});
