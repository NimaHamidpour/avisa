<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;


// CLEAR CACHE
Route::prefix('cache')->group(function (){
    Route::get('/config', function () {
        $exit_code = Artisan::call('config:cache');
        return 'true';
    });
    Route::get('/view', function () {
        $exit_code = Artisan::call('view:cache');
        return 'true';
    });
    Route::get('/clear', function () {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });
});


// API
Route::prefix('Api')->namespace('Api')->name('api.')->group(function() {

    // LOAD BLOG
    Route::post('/loadBlog','BlogController@blogGroups')->name('load.blog');


    // BLOG SEARCH
    Route::post('/Blog/search', 'BlogController@search')->name('blog.search');

    // LIKE AND DISLIKE COMMENT
    Route::post('/Comment/like', 'CommentController@like')->name('comment.like');
    Route::post('/Comment/dislike', 'CommentController@dislike')->name('comment.dislike');


});


// APP
Route::namespace('App')->name('app.')->group(function ()
{
    // HOME
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/page/{page}', 'PageController@show')->name('page.show');

    // SERVICE
    Route::prefix('service')->name('service.')->group(function ()
    {
        Route::get('/show/{service}','ServiceController@show')->name('show');
    });

    // Country
    Route::prefix('country')->name('country.')->group(function ()
    {
        Route::get('/show/{country}','CountryController@show')->name('show');

    });


    // BLOG
    Route::prefix('blog+')->name('blog.')->group(function ()
    {
        Route::get('/{slug?}', 'BlogController@index')->name('home');
        Route::get('/article/{page}', 'BlogController@show')->name('show');
        Route::post('/search', 'BlogController@search')->name('search');
        Route::post('/comment/store', 'CommentController@store')->name('comment.store');
    });

});



// LOGIN ADMIN
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::prefix('admin-panel')->namespace('Auth')->name('admin.')->group(function ()
{
    Route::get('/login', 'AdminController@show')->name('login.show');
    Route::post('/login', 'AdminController@submit')->name('login.submit');

    Route::get('/forget', 'AdminController@forgetform')->name('forget.form');
    Route::post('/forget', 'AdminController@forget')->name('forget');

    Route::get('/reset/{email}', 'AdminController@resetform')->name('reset.form');
    Route::post('/reset', 'AdminController@reset')->name('reset');

    Route::get('logout', 'AdminController@logout')->name('logout');
});



// ADMIN
Route::prefix('admin-panel')->namespace('Admin')->name('admin.')
    ->middleware('auth:admin')->group(function () {


        Route::get('/', 'IndexController@index')->name('dashboard');


        //************ admin  *************
        Route::prefix('admin')->name('admin.')->group(function () {
            Route::get('/edit', 'AdminController@edit')->name('edit');
            Route::put('/update/{user}', 'AdminController@update')->name('update');

            Route::get('/changepassword', 'AdminController@edit_passwrod')->name('edit.password');
            Route::put('/updatepassword', 'AdminController@update_passwrod')->name('update.password');
        });


        //************ user admin *************
        Route::prefix('user')->name('user.')->group(function () {

            Route::get('/list', 'UserController@index')->name('list');

            Route::get('/add', 'UserController@create')->name('create');
            Route::post('/store', 'UserController@store')->name('store');

            Route::get('/edit/{user}', 'UserController@edit')->name('edit');
            Route::post('/update/{user}', 'UserController@update')->name('update');

            Route::get('/show/{user}', 'UserController@show')->name('show');
            Route::post('/search', 'UserController@search')->name('search');
        });


        //************ page admin *************
        Route::prefix('page')->name('page.')->group(function () {
            Route::get('/list', 'PageController@index')->name('list');
            Route::get('/add', 'PageController@create')->name('create');
            Route::post('/store', 'PageController@store')->name('store');
            Route::get('/edit/{page}', 'PageController@edit')->name('edit');
            Route::post('/update/{page}', 'PageController@update')->name('update');
            Route::get('/destroy/{page}', 'PageController@destroy')->name('destroy');
        });


        //************ blog admin *************
        Route::prefix('blog')->name('blog.')->group(function () {
            Route::get('/list', 'BlogController@index')->name('list');
            Route::get('/add', 'BlogController@create')->name('create');
            Route::post('/store', 'BlogController@store')->name('store');
            Route::get('/edit/{page}', 'BlogController@edit')->name('edit');
            Route::post('/update/{page}', 'BlogController@update')->name('update');
            Route::get('/destroy/{page}', 'BlogController@destroy')->name('destroy');
        });


        //************ country admin *************
        Route::prefix('country')->name('country.')->group(function () {
            Route::get('/list', 'CountryController@index')->name('list');
            Route::get('/add', 'CountryController@create')->name('create');
            Route::post('/store', 'CountryController@store')->name('store');
            Route::get('/edit/{country}', 'CountryController@edit')->name('edit');
            Route::post('/update/{country}', 'CountryController@update')->name('update');
            Route::get('/destroy/{country}', 'CountryController@destroy')->name('destroy');
        });


        //************ service admin *************
        Route::prefix('service')->name('service.')->group(function () {
            Route::get('/list', 'ServiceController@index')->name('list');
            Route::get('/add', 'ServiceController@create')->name('create');
            Route::post('/store', 'ServiceController@store')->name('store');
            Route::get('/edit/{service}', 'ServiceController@edit')->name('edit');
            Route::post('/update/{service}', 'ServiceController@update')->name('update');
            Route::get('/destroy/{service}', 'ServiceController@destroy')->name('destroy');
        });


        //************ category admin *************
        Route::prefix('category')->name('category.')->group(function () {
            Route::get('/list/{type}', 'CategoryController@index')->name('list');
            Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
            Route::post('/update/{id}', 'CategoryController@update')->name('update');
            Route::post('/add', 'CategoryController@store')->name('store');
            Route::get('/destroy/{id}', 'CategoryController@destroy')->name('destroy');
        });


        //************ setting admin *************
        Route::name('setting.')->group(function () {
            Route::get('/setting', 'SettingController@edit')->name('edit');
            Route::post('/setting', 'SettingController@update')->name('update');
        });


        // ****************** social ***********
        Route::prefix('social')->name('social.')->group(function() {
            Route::get('list', 'SocialController@index')->name('list');
            Route::post('store', 'SocialController@store')->name('store');
            Route::get('destroy/{meta}', 'SocialController@destroy')->name('destroy');
        });


        //************ about admin *************
        Route::prefix('aboutus')->name('aboutus.')->group(function () {
            Route::get('/aboutus', 'AboutController@index')->name('list');
            Route::post('/aboutus/store', 'AboutController@store')->name('store');
            Route::get('/aboutus/destroy/{id}', 'AboutController@destroy')->name('destroy');
        });


        //************ service admin *************
        Route::prefix('menu')->name('menu.')->group(function () {
            Route::get('/list', 'MenuController@index')->name('list');
            Route::post('/store', 'MenuController@store')->name('store');
            Route::get('/edit/{menu}', 'MenuController@edit')->name('edit');
            Route::post('/update/{menu}', 'MenuController@update')->name('update');
            Route::get('/destroy/{menu}', 'MenuController@destroy')->name('destroy');
        });

        //************ image admin *************
        Route::prefix('image')->name('image.')->group(function() {
            Route::get('list', 'ImageController@index')->name('list');
            Route::post('store', 'ImageController@store')->name('store');
            Route::get('edit/{image}', 'ImageController@edit')->name('edit');
            Route::post('update/{image}', 'ImageController@update')->name('update');
            Route::get('destroy/{image}', 'ImageController@destroy')->name('destroy');
        });


    });

// FILE MANAGER
Route::group(['prefix' => 'filemanager', 'middleware' => ['web', 'auth:admin']], function () {
    Lfm::routes();
});
