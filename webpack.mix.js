const mix = require('laravel-mix');


mix.styles([
    'resources/css/bootstrap/bootstrap.css',
    'resources/css/bootstrap/bootstrap-grid.min.css',
    'resources/css/bootstrap/sb-admin.min.css',
    'resources/css/google/fontgoogleapi.css',
    'resources/css/google/fontgoogleapi2.css',
    'resources/css/style/basic.css',
    'resources/css/style/style.css',
    'resources/css/style/admin.css',
    'resources/css/style/user.css',
    'resources/css/style/blog.css',
    'resources/css/style/flag.css',
    'resources/css/slider/owl.carousel.min.css',
    'resources/css/slider/owl.theme.default.min.css',
    'resources/css/other/aos.css',
    'resources/css/persianDatePicker/persian-datepicker.css',
    'resources/css/tagify/tagify.css',

],'public/css/app2.css');

mix.scripts([
    'resources/js/vendor/jquery/jquery.min.js',
    'resources/js/vendor/bootstrap/js/bootstrap.bundle.min.js',
    'resources/js/vendor/jquery-easing/jquery.easing.min.js',
    'resources/js/vendor/magnific-popup/jquery.magnific-popup.min.js',
    'resources/js/counter/counter3.js',
    'resources/js/counter/counter.js',
    'resources/js/counter/counter2.js',
    'resources/js/admin/sb-admin.min.js',
    'resources/js/slider/owl.carousel.min.js',
    'resources/js/aos/aos.js',
    'resources/js/aos/aosnext.js',
    'resources/js/other/sweetalert.js',
    'resources/js/other/easyzoom.js',
    'resources/js/persianDatePicker/persian-date.js',
    'resources/js/persianDatePicker/persian-datepicker.js',
    'resources/js/tagify/tagify.min.js',
    'resources/js/custom/blog.js',
    'resources/js/custom/home.js',


],'public/js/app2.js');
